package com.app.reparerprovider.helpers;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by user on 26-10-2017.
 */

public class AppSettings {
    Context context;
    String isLogged;
    String token;
    String fireBaseToken;
    String imageUploadPath;
    String pageNumber;

    public String getImageUploadPath() {
        imageUploadPath = SharedHelper.getKey(context, "imageUploadPath");

        return imageUploadPath;
    }

    public void setImageUploadPath(String imageUploadPath) {
        SharedHelper.putKey(context, "imageUploadPath", imageUploadPath);

        this.imageUploadPath = imageUploadPath;
    }

    public String getProviderId() {
        providerId = SharedHelper.getKey(context, "providerId");

        return providerId;
    }

    public void setProviderId(String providerId) {
        SharedHelper.putKey(context, "providerId", providerId);

        this.providerId = providerId;
    }

    String providerId;
    JSONArray registerArray = new JSONArray();
    JSONArray timeSlots = new JSONArray();
    JSONArray availableSlots = new JSONArray();


    public JSONArray getRegisterArray() throws JSONException {
        registerArray = new JSONArray(SharedHelper.getKey(context, "registerArray"));

        return registerArray;
    }

    public void setRegisterArray(JSONArray registerArray) {
        SharedHelper.putKey(context, "registerArray", registerArray.toString());

        this.registerArray = registerArray;
    }


    public JSONArray getAvailableSlots() throws JSONException {
        availableSlots = new JSONArray(SharedHelper.getKey(context, "registerArray"));

        return availableSlots;
    }

    public void setAvailableSlots(JSONArray availableSlots) {
        SharedHelper.putKey(context, "availableSlots", availableSlots.toString());

        this.availableSlots = availableSlots;
    }

    public JSONArray getTimeSlots() {
        try {
            timeSlots = new JSONArray(SharedHelper.getKey(context, "timeSlots"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return timeSlots;
    }

    public void setTimeSlots(JSONArray timeSlots) {
        SharedHelper.putKey(context, "timeSlots", timeSlots.toString());
        this.timeSlots = timeSlots;
    }

    public String getPageNumber() {
        pageNumber = SharedHelper.getKey(context, "pageNumber");

        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        SharedHelper.putKey(context, "pageNumber", pageNumber);

        this.pageNumber = pageNumber;
    }

    public String getIsLogged() {
        isLogged = SharedHelper.getKey(context, "isLogged");
        return isLogged;
    }

    public void setIsLogged(String isLogged) {
        SharedHelper.putKey(context, "isLogged", isLogged);
        this.isLogged = isLogged;
    }

    public AppSettings(Context context) {

        this.context = context;
    }

    public String getToken() {
        token = SharedHelper.getKey(context, "token");
        return token;
    }

    public void setToken(String token) {
        SharedHelper.putKey(context, "token", token);
        this.token = token;
    }

    public String getFireBaseToken() {
        fireBaseToken = SharedHelper.getKey(context, "fireBaseToken");
        return fireBaseToken;
    }

    public void setFireBaseToken(String fireBaseToken) {
        SharedHelper.putKey(context, "fireBaseToken", fireBaseToken);
        this.fireBaseToken = fireBaseToken;
    }

}
