package com.app.reparerprovider.helpers;

/**
 * Created by user on 23-10-2017.
 */

/**
 * Created by user on 23-10-2017.
 */
public class UrlHelper {

    public static String BASE = "http://52.91.252.71/";
    public static String BASE_URL = BASE + "uber_test/public/provider/";
    public static String SIGN_UP = BASE_URL + "provider_signup";
    public static String SIGN_IN = BASE_URL + "providerlogin";
    public static String LIST_CATEGORY = BASE_URL + "listcategory";
    public static String LIST_SUB_CATEGORY = BASE_URL + "listsubcategory";
    public static String FORGOT_PASSWORD = BASE_URL + "forgotpassword";

    public static String APP_SETTINGS = BASE_URL + "appsettings";
    public static String HOME_DASHBOARD = BASE_URL + "homedashboard";
    public static String UPDATE_DEVICE_TOKEN = BASE_URL + "updatedevicetoken";
    public static String ACCEPT_JOB = BASE_URL + "acceptbooking";
    public static String REJECT_JOB = BASE_URL + "rejectbooking";
    public static String START_JOB = BASE_URL + "startedjob";
    public static String START_TO_PLACE = BASE_URL + "starttocustomerplace";
    public static String COMPLETED_JOB = BASE_URL + "completedjob";
    public static String PAYMENT_CONFIRMATION = BASE_URL + "paymentaccept";
    public static String REVIEW = BASE_URL + "userreviews";
    public static String CANCEL_BOOKING = BASE_URL + "cancelbyprovider";

    public static String VIEW_PROFILE = BASE_URL + "viewprofile";
    public static String VIEW_CATEGORY = BASE_URL + "view_provider_category";
    public static String VIEW_SCHEDULES = BASE_URL + "view_schedules";
    public static String UPDATE_SCHEDULES = BASE_URL + "updateschedules";
    public static String EDIT_CATEGORY = BASE_URL + "edit_category";
    public static String DELETE_CATEGORY = BASE_URL + "delete_category";
    public static String ADD_CATEGORY = BASE_URL + "add_category";
    public static String RESET_PASSWORD = BASE_URL + "resetpassword";
    public static String UPLOAD_IMAGE = BASE + "uber_test/public/admin/imageupload";

    public static String UPDATE_LOCATION = BASE_URL + "update_location";
    public static String ABOUT_US = BASE_URL + "aboutus";
    public static String FAQ = BASE + "uber_test/public/admin/faq";
    public static String TERMS = BASE + "uber_test/public/admin/terms";
    public static String CHANGE_PASSWORD = BASE_URL + "changepassword";
    public static String UPDATE_ADDRESS = BASE_URL + "update_address";
    public static String UPDATE_PROFILE = BASE_URL + "updateprofile";
    public static String SOCKET_URL = "http://52.91.252.71:3000/";

}