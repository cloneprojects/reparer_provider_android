package com.app.reparerprovider.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;


import com.app.reparerprovider.R;
import com.app.reparerprovider.fragments.HomeFragment;
import com.app.reparerprovider.fragments.PageFragment;

import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by karthik on 01/10/17.
 */
public class OnboardActivity extends BaseActivity {
    String[] titles, subtitles;
    int[] backgrounds;
    public static String TAG = HomeFragment.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_onboard);
        titles = new String[]{getResources().getString(R.string.uberdoo), getResources().getString(R.string.photographer), getResources().getString(R.string.interior_design), getResources().getString(R.string.beautician)};
//        subtitles = new String[]{"One Stop Solution for all your needs. Jump right in.","Get your parcels delivered anywhere with a simple click.","Get your personal health advice from our medical experts.","Broken things lying around? Worry not. We'll fix your things."};
        subtitles = new String[]{getResources().getString(R.string.text_one), getResources().getString(R.string.text_two), getResources().getString(R.string.text_three), getResources().getString(R.string.text_four)};
        backgrounds = new int[]{R.drawable.onboard_1, R.drawable.onboard_2, R.drawable.onboard_3, R.drawable.onboard_4};
        ViewPager pager = (ViewPager) findViewById(R.id.onBoardPager);


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setOffscreenPageLimit(1);
        pager.setAdapter(adapter);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(pager);


        Button signIn = (Button) findViewById(R.id.signIn);
        Button signUp = (Button) findViewById(R.id.signUp);
        Button skipLogin = (Button) findViewById(R.id.skipLogin);
        skipLogin.setVisibility(View.GONE);

//        Intent main = new Intent(OnboardActivity.this,MainActivity.class);
//        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(main);
//        finish();


        skipLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent main = new Intent(OnboardActivity.this, MainActivity.class);
                main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(main);
                finish();
            }
        });
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signIn = new Intent(OnboardActivity.this, SignInActivity.class);
                startActivity(signIn);
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signUp = new Intent(OnboardActivity.this, RegisterActivity.class);
                startActivity(signUp);
//                showReviewPending(OnboardActivity.this);
            }
        });

    }


    public class ViewPagerAdapter extends FragmentStatePagerAdapter {


        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PageFragment.newInstance(titles[position], subtitles[position], backgrounds[position], position);
        }

        @Override
        public int getCount() {
            return titles.length;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 106) {
            if (grantResults.length > 0) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
        }
    }

}
