package com.app.reparerprovider.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.app.reparerprovider.R;
import com.app.reparerprovider.Volley.ApiCall;
import com.app.reparerprovider.Volley.VolleyCallback;
import com.app.reparerprovider.helpers.DataParser;
import com.app.reparerprovider.helpers.SharedHelper;
import com.app.reparerprovider.helpers.Utils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MapActivity extends BaseActivity implements OnMapReadyCallback, LocationListener {

    GoogleMap map;
    private LocationManager locationManager;
    TextView providerName, distanceView, etaView;
    ImageView callButton;
    private String dist, dura;
    String mobileNumber;
    private String TAG = MapActivity.class.getSimpleName();
    private LatLng origin;
    private LatLng dest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(MapActivity.this, "theme_value");
        Utils.SetTheme(MapActivity.this, theme_value);
        setContentView(R.layout.activity_map);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        ImageView providerPic = (ImageView) findViewById(R.id.providerPic);
        Utils.setProfilePicture(MapActivity.this, providerPic);
        mobileNumber = getIntent().getStringExtra("mobileNumber");

        providerName = (TextView) findViewById(R.id.providerName);
        distanceView = (TextView) findViewById(R.id.distance);
        etaView = (TextView) findViewById(R.id.eta);
        callButton = (ImageView) findViewById(R.id.callButton);
        String pro_name = getIntent().getStringExtra("providerName");
        providerName.setText(pro_name);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 106);
            }
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);
        ImageView backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts(
                        "tel", mobileNumber, null));
                startActivity(phoneIntent);
            }
        });


    }

    private String getDistanceInfo(String src_lat, String src_lng, String des_lat, String des_lng) {
        try {

            String url = "http://maps.googleapis.com/maps/api/directions/json?origin=" + src_lat + "," + src_lng + "&destination=" + des_lat + "," + des_lng + "&mode=driving&sensor=false?key=" + getResources().getString(R.string.google_maps_key);
            Utils.log(TAG, "url: " + url);

            ApiCall.getMethod(MapActivity.this, url, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject = response;
                    try {
                        Utils.log(TAG, "jsonObject: " + response);
                        JSONArray array = response.optJSONArray("routes");
                        JSONObject routes = array.getJSONObject(0);
                        JSONArray legs = routes.getJSONArray("legs");
                        JSONObject steps = legs.getJSONObject(0);
                        JSONObject distance = steps.getJSONObject("distance");
                        JSONObject duration = steps.getJSONObject("duration");

                        Log.i("Distance", distance.toString());
                        dist = distance.getString("text");
                        dura = duration.getString("text");
                        distanceView.setText(dist);
                        etaView.setText(dura);

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {

        }
        return dist;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onLocationChanged(Location location) {
        CameraUpdate center =
                CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),
                        location.getLongitude()));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
//        map.moveCamera(center);
//        map.animateCamera(zoom);

        getDistanceInfo("" + location.getLatitude(), "" + location.getLongitude(), getIntent().getStringExtra("des_lat"), getIntent().getStringExtra("des_lng"));
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                getLocationFromServer();
                handler.postDelayed(this, 30000);
            }
        });
    }


    private void getLocationFromServer() {


        double des_lat = Double.parseDouble(getIntent().getStringExtra("des_lat"));
        double des_lng = Double.parseDouble(getIntent().getStringExtra("des_lng"));
        origin = new LatLng(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude());
        dest = new LatLng(des_lat, des_lng);
        String url = getUrl(origin, dest);
        FetchUrl FetchUrl = new FetchUrl();

        FetchUrl.execute(url);

    }


    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLACK);

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }
            int height = 150;
            int width = 150;
            BitmapDrawable srcBitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.tracl_location_2);
            BitmapDrawable desBitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.track_location);
            Bitmap b = srcBitmap.getBitmap();
            Bitmap ab = desBitmap.getBitmap();
            Bitmap srcsmallMarker = Bitmap.createScaledBitmap(b, width, height, false);
            Bitmap dessmallMarker = Bitmap.createScaledBitmap(ab, width, height, false);

            MarkerOptions srcmarkerOptions = new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromBitmap(srcsmallMarker));
            MarkerOptions desmarkerOptions = new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromBitmap(dessmallMarker));

            // Setting position on the MarkerOptions


            srcmarkerOptions.position(origin);
            desmarkerOptions.position(dest);
            LatLngBounds.Builder builder = new LatLngBounds.Builder();

//the include method will calculate the min and max bound.
            builder.include(srcmarkerOptions.getPosition());
            builder.include(desmarkerOptions.getPosition());
            LatLngBounds bounds = builder.build();
            int widths = getResources().getDisplayMetrics().widthPixels;
            int heights = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (widths * 0.10);
            // Animating to the currently touched position
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, widths, heights, padding);

            map.animateCamera(cu);
            map.addMarker(srcmarkerOptions);
            map.addMarker(desmarkerOptions);

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                map.addPolyline(lineOptions);
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }


    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "?key=" + getResources().getString(R.string.google_maps_key);


        return url;
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 106) {
            if (grantResults.length > 0) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);
            }
        }
    }
}
