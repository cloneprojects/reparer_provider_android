package com.app.reparerprovider.activities;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.app.reparerprovider.R;
import com.app.reparerprovider.Volley.ApiCall;
import com.app.reparerprovider.Volley.VolleyCallback;
import com.app.reparerprovider.helpers.UrlHelper;

import org.json.JSONArray;
import org.json.JSONObject;


public class AboutUs extends BaseActivity {
    WebView webView;
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        webView = (WebView) findViewById(R.id.webView);
        backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ApiCall.getMethod(AboutUs.this, UrlHelper.ABOUT_US, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray static_pages = response.optJSONArray("static_pages");
                JSONObject jsonObject = new JSONObject();

                String url = jsonObject.optString("page_url");
                webView.getSettings().setJavaScriptEnabled(false);
                webView.loadUrl(url);
                webView.setHorizontalScrollBarEnabled(false);
            }
        });


    }
}
