package com.app.reparerprovider.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.reparerprovider.R;
import com.app.reparerprovider.Volley.ApiCall;
import com.app.reparerprovider.Volley.VolleyCallback;
import com.app.reparerprovider.helpers.HoloCircularProgressBar;
import com.app.reparerprovider.helpers.SharedHelper;
import com.app.reparerprovider.helpers.UrlHelper;
import com.app.reparerprovider.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ShowTimeActivity extends BaseActivity {
    Button finishJob;

    BottomSheetDialog bottomSheetDialog;
    BottomSheetBehavior bottomSheetBehavior;
    View bottomSheetView;
    ImageView backButton;
    TextView yesButton, noButton, timerText, providerName, bookingTime, subCategoryName;
    HoloCircularProgressBar circularProgressBar;
    int rating = 0;
    Date fromDate;
    JSONObject jsonObject = new JSONObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(ShowTimeActivity.this, "theme_value");
        Utils.SetTheme(ShowTimeActivity.this, theme_value);
        setContentView(R.layout.activity_show_time);
        finishJob = (Button) findViewById(R.id.finishJob);
        Utils.setCustomButton(ShowTimeActivity.this, finishJob);
        timerText = (TextView) findViewById(R.id.timerText);
        providerName = (TextView) findViewById(R.id.providerName);
        bookingTime = (TextView) findViewById(R.id.bookingTime);
        subCategoryName = (TextView) findViewById(R.id.subCategoryName);
        backButton = (ImageView) findViewById(R.id.backButton);

        if (getIntent() != null) {
            try {
                jsonObject = new JSONObject(getIntent().getStringExtra("bookingValues"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        circularProgressBar = (HoloCircularProgressBar) findViewById(R.id.circularProgressBar);
        circularProgressBar.setProgressColor(Utils.getPrimaryColor(ShowTimeActivity.this));
        Date date = new Date();

        try {
            providerName.setText(jsonObject.optString("provider_name"));
            subCategoryName.setText(jsonObject.optString("sub_category_name"));
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            fromDate = fmt.parse(jsonObject.optString("booking_date") + " " + jsonObject.optString("job_start_time"));


        } catch (Exception e) {

        }

        finishJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFinishDialog();
            }
        });
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long yourmilliseconds = System.currentTimeMillis();
                try {
                    DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date resultdate = new Date(yourmilliseconds);
                    String date = sdf.format(resultdate);
                    Date finaldate = sdf.parse(date);
                    String difference = getDifference(fromDate, finaldate);
                    timerText.setText(difference);
                    rating = rating + 1;
                    Calendar calendar = Calendar.getInstance();
                    int seconds = calendar.get(Calendar.SECOND);
                    float f = (float) (seconds * 1.67);
                    f = (float) (f / 100.0);
                    Utils.log("values", "total: " + f);
                    circularProgressBar.setProgress(f);
                } catch (Exception e) {

                }
                handler.postDelayed(this, 1000);
            }
        });

        try {
            providerName.setText(jsonObject.optString("provider_name"));
            subCategoryName.setText(jsonObject.optString("sub_category_name"));
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            Date outDate = dateFormatter.parse(jsonObject.optString("booking_date"));
            DateFormat formatter = new SimpleDateFormat("MMM dd yyyy");
            String dateVal = formatter.format(outDate);
            bookingTime.setText(dateVal);
        } catch (Exception e) {

        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    public String getDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();
        long totalMinutes = TimeUnit.MILLISECONDS.toMinutes(different);
        ;

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;


        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        return String.valueOf(totalMinutes);
    }

    private void showFinishDialog() {
        final Dialog dialog = new Dialog(ShowTimeActivity.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.job_completed_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        yesButton = (TextView) dialog.findViewById(R.id.yesButton);
        noButton = (TextView) dialog.findViewById(R.id.noButton);
        ImageView ok_completed = (ImageView) dialog.findViewById(R.id.ok_completed);
        Utils.setIconColour(ShowTimeActivity.this, ok_completed);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                try {
                    hitFinishJob();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }


    public void hitFinishJob() throws JSONException {
        JSONObject jsonObjects = new JSONObject();
        jsonObjects.put("id", jsonObject.optString("id"));
        ApiCall.PostMethodHeaders(ShowTimeActivity.this, UrlHelper.COMPLETED_JOB, jsonObjects, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                moveMainActivity();

            }
        });
    }

    private void moveMainActivity() {
        Intent intent = new Intent(ShowTimeActivity.this, MainActivity.class);
        startActivity(intent);
    }

    public void showPaymentConfirmation() {
        bottomSheetView = getLayoutInflater().inflate(R.layout.payment_confirmation, null);
        bottomSheetDialog = new BottomSheetDialog(ShowTimeActivity.this);
        bottomSheetDialog.setContentView(bottomSheetView);
        bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
        bottomSheetDialog.show();
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        Button confirmButton = (Button) bottomSheetDialog.findViewById(R.id.confirmButton);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
    }
}
