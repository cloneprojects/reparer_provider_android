package com.app.reparerprovider.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;


import com.app.reparerprovider.R;
import com.app.reparerprovider.Volley.ApiCall;
import com.app.reparerprovider.Volley.VolleyCallback;
import com.app.reparerprovider.helpers.SharedHelper;
import com.app.reparerprovider.helpers.UrlHelper;
import com.app.reparerprovider.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {
    Button sendOtp;
    EditText emailEditText;
    ImageView backButton;

    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(ForgotPasswordActivity.this, "theme_value");
        Utils.SetTheme(ForgotPasswordActivity.this, theme_value);
        setContentView(R.layout.activity_forgot_password);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }

        initViews();
        initListners();
    }

    private void initListners() {
        backButton.setOnClickListener(this);
        sendOtp.setOnClickListener(this);

    }

    private void initViews() {
        backButton = (ImageView) findViewById(R.id.backButton);
        sendOtp = (Button) findViewById(R.id.sendOtp);
        Utils.setCustomButton(ForgotPasswordActivity.this, sendOtp);
        Button email = (Button) findViewById(R.id.email_forgot);
        Utils.setButton(ForgotPasswordActivity.this, email);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
    }

    @Override
    public void onClick(View view) {

        if (view == backButton) {
            finish();
        } else if (view == sendOtp) {
            if (isValidInputs().equalsIgnoreCase("true")) {
                requestOtp();
            } else {
                Utils.toast(ForgotPasswordActivity.this, isValidInputs());
            }
        }
    }

    private String isValidInputs() {

        String val;
        if (emailEditText.getText().length() == 0 || !isValidEmail(emailEditText.getText().toString().trim())) {
            val = getResources().getString(R.string.please_enter_valid_email);
        } else if (emailEditText.getText().toString().equalsIgnoreCase("karthik@pyrmaidions.com")) {
            val = getResources().getString(R.string.you_cannot_change_password);

        } else {
            val = "true";
        }
        return val;
    }

    private void requestOtp() {
        ApiCall.PostMethod(ForgotPasswordActivity.this, UrlHelper.FORGOT_PASSWORD, getInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                String otp = response.optString("otp");
                moveVerfication(otp);
            }


        });

    }

    private void moveVerfication(String otp) {
        Intent intent = new Intent(ForgotPasswordActivity.this, EnterVerificationCodeActivity.class);
        intent.putExtra("otp", otp);
        intent.putExtra("emailValue", emailEditText.getText().toString());
        startActivity(intent);
    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", emailEditText.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
