package com.app.reparerprovider.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.app.reparerprovider.R;
import com.app.reparerprovider.Volley.ApiCall;
import com.app.reparerprovider.Volley.VolleyCallback;
import com.app.reparerprovider.helpers.SharedHelper;
import com.app.reparerprovider.helpers.UrlHelper;
import com.app.reparerprovider.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailedBookActivitys extends BaseActivity {
    JSONObject bookingValues;
    ImageView backButton;
    LinearLayout billLayout;
    RelativeLayout activity_detailed_booking;
    ImageView profilePic;
    ImageView navigateButton;
    TextView billingName, bookingId, bokkingDate, bookingTime, bookingAddress, workingHours, pricePerhour, serviceTotal, serviceName, providerName;
    ImageView mapData;
    TextView statusText;
    RelativeLayout topBillingLayout;

    private Date fromDate;

    View viewOne, viewTwo, viewThree, viewFour, viewFive, viewSix, viewSeven, viewEight, viewNine, viewTen, viewEleven;
    LinearLayout layOne, layTwo, layThree, layFour, layFive, laySix;
    TextView textOne, textTwo, textThree, textFour, textFive, textSix;
    ImageView timeOne, timeTwo, timeThree, timeFour, timeFive, timeSix;
    String statusOne = "", statusTwo = "", statusThree = "", statusFour = "", statusFive = "", statusSix = "";

    TextView totalCostandTime, dummyText, hoursValue;
    TextView bookingGst, taxName, taxPercentage;
    private String TAG = DetailedBookActivitys.class.getSimpleName();
    private TextView yesButton;
    private TextView noButton;
    private Button cancel, startPlace;

    public static String formatHoursAndMinutes(int totalMinutes) {
        String minutes = Integer.toString(totalMinutes % 60);
        minutes = minutes.length() == 1 ? "0" + minutes : minutes;
        if (minutes.equalsIgnoreCase("00")) {
            return (totalMinutes / 60) + " hrs";
        }
        return (totalMinutes / 60) + ":" + minutes + " hrs";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(DetailedBookActivitys.this, "theme_value");
        Utils.SetTheme(DetailedBookActivitys.this, theme_value);
        setContentView(R.layout.activity_detailed_book_activitys);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        initViews();

        try {
            setValues();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        startPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String status = "";

                if (startPlace.getText().toString().equalsIgnoreCase(getResources().getString(R.string.start_to_place))) {
                    status = "updatePlace";
                }

                if (startPlace.getText().toString().equalsIgnoreCase(getResources().getString(R.string.start_job))) {
                    status = "startJob";
                }
                if (startPlace.getText().toString().equalsIgnoreCase(getResources().getString(R.string.completed))) {
                    status = "completeJob";
                }

                updateStatus(bookingValues.optString("id"), status);
            }
        });


        navigateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String status = "";
                status = "updatePlace";
//                if (bookingValues.optString("status").equalsIgnoreCase("StarttoCustomerPlace")) {
                String src = bookingValues.optString("user_latitude");
                String des = bookingValues.optString("user_longitude");
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=" + src + "," + des));
                startActivity(intent);
//                } else {
//                    updateStatus(bookingValues.optString("id"), status);
//                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCancelDialog(bookingValues.optString("id"));
            }
        });

    }

    private void showCancelDialog(final String id) {


        final Dialog dialog = new Dialog(DetailedBookActivitys.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.cancel_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        TextView yesButton, noButton;
        yesButton = (TextView) dialog.findViewById(R.id.yesButton);
        noButton = (TextView) dialog.findViewById(R.id.noButton);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                cancelRequest(id);
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }

    private void cancelRequest(String id) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodHeaders(DetailedBookActivitys.this, UrlHelper.CANCEL_BOOKING, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                finish();
            }
        });

    }

    public void hitFinishJob() throws JSONException {
        JSONObject jsonObjects = new JSONObject();
        jsonObjects.put("id", bookingValues.optString("id"));
        ApiCall.PostMethodHeaders(DetailedBookActivitys.this, UrlHelper.COMPLETED_JOB, jsonObjects, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                moveMainActivity();

            }
        });
    }

    private void moveMainActivity() {
        Intent intent = new Intent(DetailedBookActivitys.this, MainActivity.class);
        intent.putExtra("type", "false");
        startActivity(intent);
        finish();
    }


    private void updateStatus(String id, String status) {
        String url = "";
        if (status.equalsIgnoreCase("updatePlace")) {
            url = UrlHelper.START_TO_PLACE;
        }

        if (status.equalsIgnoreCase("startJob")) {
            url = UrlHelper.START_JOB;
        }
        if (status.equalsIgnoreCase("completeJob")) {
            url = UrlHelper.COMPLETED_JOB;

        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodHeaders(DetailedBookActivitys.this, url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                moveMainActivity();
            }
        });


    }

    private void showFinishDialog() {
        final Dialog dialog = new Dialog(DetailedBookActivitys.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.job_completed_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        yesButton = (TextView) dialog.findViewById(R.id.yesButton);
        noButton = (TextView) dialog.findViewById(R.id.noButton);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                try {
                    hitFinishJob();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }


    private void trackPage() {

        String src = bookingValues.optString("user_latitude");
        String des = bookingValues.optString("user_longitude");
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + src + "," + des));
        startActivity(intent);

    }


    private void initViews() {

        backButton = (ImageView) findViewById(R.id.backButton);
        billingName = (TextView) findViewById(R.id.billingName);
        bookingId = (TextView) findViewById(R.id.bookingId);
        bokkingDate = (TextView) findViewById(R.id.bokkingDate);
        hoursValue = (TextView) findViewById(R.id.hoursValue);
//        bookingTime = (TextView) findViewById(R.id.bookingTime);
        bookingAddress = (TextView) findViewById(R.id.bookingAddress);
        workingHours = (TextView) findViewById(R.id.workingHours);
        pricePerhour = (TextView) findViewById(R.id.pricePerhour);
        serviceTotal = (TextView) findViewById(R.id.serviceTotal);
        serviceName = (TextView) findViewById(R.id.serviceName);
        totalCostandTime = (TextView) findViewById(R.id.totalCostandTime);
//        dummyText = (TextView) findViewById(R.id.dummyText);
//        providerName = (TextView) findViewById(R.id.providerName);
        billLayout = (LinearLayout) findViewById(R.id.billLayout);
        profilePic = (ImageView) findViewById(R.id.profilePic);
        bookingGst = (TextView) findViewById(R.id.bookingGst);
        taxName = (TextView) findViewById(R.id.taxName);
        taxPercentage = (TextView) findViewById(R.id.taxPercentage);
        mapData = (ImageView) findViewById(R.id.mapData);
        statusText = (TextView) findViewById(R.id.statusText);
        activity_detailed_booking = (RelativeLayout) findViewById(R.id.activity_detailed_booking);

        activity_detailed_booking.setBackgroundColor(getResources().getColor(R.color.transparent));

        cancel = (Button) findViewById(R.id.cancel);
        startPlace = (Button) findViewById(R.id.startPlace);
        Utils.setCustomButton(DetailedBookActivitys.this, startPlace);

        textOne = (TextView) findViewById(R.id.textOne);
        textTwo = (TextView) findViewById(R.id.textTwo);
        textThree = (TextView) findViewById(R.id.textThree);
        textFour = (TextView) findViewById(R.id.textFour);
        textFive = (TextView) findViewById(R.id.textFive);
        textSix = (TextView) findViewById(R.id.textSix);

        layOne = (LinearLayout) findViewById(R.id.layOne);
        layTwo = (LinearLayout) findViewById(R.id.layTwo);
        layThree = (LinearLayout) findViewById(R.id.layThree);
        layFour = (LinearLayout) findViewById(R.id.layFour);
        layFive = (LinearLayout) findViewById(R.id.layFive);
        laySix = (LinearLayout) findViewById(R.id.laySix);

        timeOne = (ImageView) findViewById(R.id.timeOne);
        timeTwo = (ImageView) findViewById(R.id.timeTwo);
        timeThree = (ImageView) findViewById(R.id.timeThree);
        timeFour = (ImageView) findViewById(R.id.timeFour);
        timeFive = (ImageView) findViewById(R.id.timeFive);
        timeSix = (ImageView) findViewById(R.id.timeSix);
        navigateButton = (ImageView) findViewById(R.id.navigateButton);
        //Utils.setIconColour(DetailedBookActivitys.this, navigateButton);

        viewOne = findViewById(R.id.viewOne);
        viewTwo = findViewById(R.id.viewTwo);
        viewThree = findViewById(R.id.viewThree);
        viewFour = findViewById(R.id.viewFour);
        viewFive = findViewById(R.id.viewFive);
        viewSix = findViewById(R.id.viewSix);
        viewSeven = findViewById(R.id.viewSeven);
        viewEight = findViewById(R.id.viewEight);
        viewNine = findViewById(R.id.viewNine);
        viewTen = findViewById(R.id.viewTen);
        viewEleven = findViewById(R.id.viewEleven);


        viewOne.setBackgroundColor(Utils.getPrimaryColor(DetailedBookActivitys.this));
        viewTwo.setBackgroundColor(Utils.getPrimaryColor(DetailedBookActivitys.this));
        viewThree.setBackgroundColor(Utils.getPrimaryColor(DetailedBookActivitys.this));
        viewFour.setBackgroundColor(Utils.getPrimaryColor(DetailedBookActivitys.this));
        viewFive.setBackgroundColor(Utils.getPrimaryColor(DetailedBookActivitys.this));
        viewSix.setBackgroundColor(Utils.getPrimaryColor(DetailedBookActivitys.this));
        viewSeven.setBackgroundColor(Utils.getPrimaryColor(DetailedBookActivitys.this));
        viewEight.setBackgroundColor(Utils.getPrimaryColor(DetailedBookActivitys.this));
        viewNine.setBackgroundColor(Utils.getPrimaryColor(DetailedBookActivitys.this));
        viewTen.setBackgroundColor(Utils.getPrimaryColor(DetailedBookActivitys.this));
        viewEleven.setBackgroundColor(Utils.getPrimaryColor(DetailedBookActivitys.this));

        topBillingLayout = (RelativeLayout) findViewById(R.id.topBillingLayout);


    }

    public String convertedTime(String time) throws ParseException {
        String ftime = "";
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date outDate = dateFormatter.parse(time);
        DateFormat formatter = new SimpleDateFormat("d MMM, yyyy HH:mm a");
        ftime = formatter.format(outDate);

        return ftime;
    }

    private void setValues() throws JSONException, ParseException {
        bookingValues = new JSONObject(getIntent().getStringExtra("bookingValues"));
        Utils.log(TAG, ":bookingValues " + bookingValues);
        String status = bookingValues.optString("status");

        String showBill = bookingValues.optString("show_bill_flag");
//        if (!bookingValues.optString("status").equalsIgnoreCase("Finished")) {
        if (showBill.equalsIgnoreCase("0")) {
            billLayout.setVisibility(View.GONE);
            totalCostandTime.setVisibility(View.GONE);
//            dummyText.setVisibility(View.GONE);
            topBillingLayout.setVisibility(View.GONE);
            billLayout.setVisibility(View.GONE);
            bookingGst.setVisibility(View.GONE);
            taxName.setVisibility(View.GONE);
            taxPercentage.setVisibility(View.GONE);
        } else {
            topBillingLayout.setVisibility(View.VISIBLE);

            billLayout.setVisibility(View.VISIBLE);
//            dummyText.setVisibility(View.VISIBLE);
            totalCostandTime.setVisibility(View.VISIBLE);
            int total = Integer.parseInt(bookingValues.optString("worked_mins"));
            String hours = formatHoursAndMinutes(total);
//            workingHours.setText(hours);
//            serviceTotal.setText("$" + bookingValues.optString("total_cost"));

            totalCostandTime.setText("$" + bookingValues.optString("total_cost"));
            hoursValue.setText(hours);

        }
//        } else {
//            serviceTotal.setText("$" + bookingValues.optString("cost"));
//            int total = Integer.parseInt(bookingValues.optString("worked_mins"));
//            String hours = formatHoursAndMinutes(total);
//            workingHours.setText(hours);
//
//        }
        billingName.setText(bookingValues.optString("Name"));
        bookingId.setText(bookingValues.optString("booking_order_id"));
        bokkingDate.setText(bookingValues.optString("booking_date") + " " + bookingValues.optString("timing"));
//        bookingTime.setText(bookingValues.optString("timing"));
        bookingAddress.setText(bookingValues.optString("address_line_1"));
        pricePerhour.setText("$" + bookingValues.optString("cost"));

        serviceName.setText(bookingValues.optString("sub_category_name"));
//        providerName.setText(bookingValues.optString("providername"));
        bookingGst.setText("$" + bookingValues.optString("gst_cost"));
        taxName.setText(bookingValues.optString("tax_name"));
        taxPercentage.setText(bookingValues.optString("gst_percent") + "%");
        statusText.setText(status.toUpperCase());


        if (bookingValues.isNull("Pending_time")) {
            statusOne = getResources().getString(R.string.service_booked);

        } else {
            statusOne = getResources().getString(R.string.service_booked) + "\n" + convertedTime(bookingValues.optString("Pending_time"));

        }


        if (bookingValues.isNull("Accepted_time")) {
            statusTwo = getResources().getString(R.string.service_accepted);


        } else {
            statusTwo = getResources().getString(R.string.service_accepted) + "\n" + convertedTime(bookingValues.optString("Accepted_time"));


        }


        if (bookingValues.isNull("StarttoCustomerPlace_time")) {
            statusThree = getResources().getString(R.string.started_to_customer_place);


        } else {
            statusThree = getResources().getString(R.string.started_to_customer_place) + "\n" + convertedTime(bookingValues.optString("StarttoCustomerPlace_time"));

        }

        if (bookingValues.isNull("startjob_timestamp")) {
            statusFour = getResources().getString(R.string.provider_arrived);
            statusFive = getResources().getString(R.string.job_started);


        } else {
            statusFour = getResources().getString(R.string.provider_arrived) + "\n" + convertedTime(bookingValues.optString("startjob_timestamp"));

            statusFive = getResources().getString(R.string.job_started) + "\n" + convertedTime(bookingValues.optString("startjob_timestamp"));

        }

        if (bookingValues.isNull("endjob_timestamp")) {
            statusSix = getResources().getString(R.string.job_completed);


        } else {
            statusSix = getResources().getString(R.string.job_completed) + "\n" + convertedTime(bookingValues.optString("endjob_timestamp"));


        }


        int colorvalue = 0;
        if (status.equalsIgnoreCase("Pending")) {
            navigateButton.setVisibility(View.GONE);
            setBooked();
            colorvalue = getResources().getColor(R.color.light_violet);
            statusText.setText(getResources().getString(R.string.cancel));


        } else if (status.equalsIgnoreCase("Rejected")) {
            navigateButton.setVisibility(View.GONE);

            statusTwo = getResources().getString(R.string.service_rejected) + "\n" + bookingValues.optString("Rejected_time");

            setAccepted();
            colorvalue = getResources().getColor(R.color.red);
            statusText.setText(getResources().getString(R.string.rejected));


        } else if (status.equalsIgnoreCase("Accepted")) {
            navigateButton.setVisibility(View.VISIBLE);

            setAccepted();
            colorvalue = getResources().getColor(R.color.light_violet);
            statusText.setText(getResources().getString(R.string.accepted));


        } else if (status.equalsIgnoreCase("CancelledbyUser")) {
            navigateButton.setVisibility(View.GONE);

            statusThree = getResources().getString(R.string.cancelled_by_user) + "\n" + bookingValues.optString("CancelledbyUser_time");

            setStartToPlace();
            colorvalue = getResources().getColor(R.color.red);
            statusText.setText(getResources().getString(R.string.cancelled));

        } else if (status.equalsIgnoreCase("CancelledbyProvider")) {
            navigateButton.setVisibility(View.GONE);


            statusThree = getResources().getString(R.string.cancelled_by_provider) + "\n" + bookingValues.optString("CancelledbyProvider_time");

            setStartToPlace();
            colorvalue = getResources().getColor(R.color.red);
            statusText.setText(getResources().getString(R.string.cancelled));


        } else if (status.equalsIgnoreCase("StarttoCustomerPlace")) {

            navigateButton.setVisibility(View.VISIBLE);

            setStartToPlace();
            colorvalue = getResources().getColor(R.color.light_violet);
            statusText.setText(getResources().getString(R.string.on_the_way));


        } else if (status.equalsIgnoreCase("Startedjob")) {
            navigateButton.setVisibility(View.GONE);

            setJobStarted();

            colorvalue = getResources().getColor(R.color.light_violet);
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            fromDate = fmt.parse(bookingValues.optString("startjob_timestamp"));

            final Handler handler = new Handler();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    long yourmilliseconds = System.currentTimeMillis();
                    try {
                        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date resultdate = new Date(yourmilliseconds);
                        String date = sdf.format(resultdate);
                        Date finaldate = sdf.parse(date);
                        getDifference(fromDate, finaldate);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    handler.postDelayed(this, 1000);
                }
            });

        } else if (status.equalsIgnoreCase("Completedjob")) {
            navigateButton.setVisibility(View.GONE);

            setJobCompleted();

            colorvalue = getResources().getColor(R.color.green);
            statusText.setText(getResources().getString(R.string.completed));
        } else if (status.equalsIgnoreCase("Waitingforpaymentconfirmation")) {
            navigateButton.setVisibility(View.GONE);

            colorvalue = getResources().getColor(R.color.status_orange);


        } else if (status.equalsIgnoreCase("Reviewpending")) {
            navigateButton.setVisibility(View.GONE);

            colorvalue = getResources().getColor(R.color.ratingColor);

        } else if (status.equalsIgnoreCase("Finished")) {
            navigateButton.setVisibility(View.GONE);

            setJobCompleted();
            colorvalue = getResources().getColor(R.color.green);
            statusText.setText(getResources().getString(R.string.completed));
        }

        statusText.setTextColor(colorvalue);


        if (status.equalsIgnoreCase("Rejected")) {
            startPlace.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);

        } else if (status.equalsIgnoreCase("CancelledbyUser")) {
            startPlace.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);


        } else if (status.equalsIgnoreCase("CancelledbyProvider")) {
            startPlace.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);


        } else if (status.equalsIgnoreCase("Pending")) {

            startPlace.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.GONE);


        } else if (status.equalsIgnoreCase("Accepted")) {
            startPlace.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.VISIBLE);
            startPlace.setText(getResources().getString(R.string.start_to_place));

        } else if (status.equalsIgnoreCase("StarttoCustomerPlace")) {
            startPlace.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.VISIBLE);
            startPlace.setText(getResources().getString(R.string.start_job));

        } else if (status.equalsIgnoreCase("Startedjob")) {

            startPlace.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.GONE);
            startPlace.setText(getResources().getString(R.string.completed));

        } else if (status.equalsIgnoreCase("Completedjob")) {
            startPlace.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);


        } else if (status.equalsIgnoreCase("Waitingforpaymentconfirmation")) {

            startPlace.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);
        } else if (status.equalsIgnoreCase("Reviewpending")) {

            startPlace.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);

        } else {
            startPlace.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);
        }
        String src = bookingValues.optString("user_latitude");
        String des = bookingValues.optString("user_longitude");
        getStaticMap(src, des);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public void getDifference(Date startDate, Date endDate) {
        //milliseconds

        String first = "";
        String second = "";
        String third = "";
        String four = "";
        String five = "";
        String six = "";
        long different = endDate.getTime() - startDate.getTime();
        long totalMinutes = TimeUnit.MILLISECONDS.toMinutes(different);


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;
        String hourFull = String.valueOf(elapsedHours);
        String minutesFull = String.valueOf(elapsedMinutes);
        String SecondsFull = String.valueOf(elapsedSeconds);
        Log.d(TAG, "getDifference: " + hourFull + "/" + minutesFull + "/" + SecondsFull);
        if (hourFull.equalsIgnoreCase("0")) {
            first = "0";
            second = "0";
        } else if (hourFull.length() == 1) {
            first = "0";
            second = String.valueOf(hourFull.charAt(0));
        } else {
            first = String.valueOf(hourFull.charAt(0));
            second = String.valueOf(hourFull.charAt(1));
        }


        if (elapsedMinutes == 0) {
            third = "0";
            four = "0";
        } else if (minutesFull.length() == 1) {
            third = "0";
            four = String.valueOf(minutesFull.charAt(0));
        } else {
            third = String.valueOf(minutesFull.charAt(0));
            four = String.valueOf(minutesFull.charAt(1));
        }


        if (elapsedSeconds == 0) {
            five = "0";
            six = "0";
        } else if (SecondsFull.length() == 1) {
            five = "0";
            six = String.valueOf(SecondsFull.charAt(0));
        } else {
            five = String.valueOf(SecondsFull.charAt(0));
            six = String.valueOf(SecondsFull.charAt(1));

        }


        statusText.setText(getResources().getString(R.string.elapsed_time) + ": " + first + second + ":" + third + four + ":" + five + six);


    }


    public void setBooked() {
        layOne.setVisibility(View.VISIBLE);
        layTwo.setVisibility(View.GONE);
        layThree.setVisibility(View.GONE);
        layFour.setVisibility(View.GONE);
        layFive.setVisibility(View.GONE);
        laySix.setVisibility(View.GONE);

        //timeOne.setImageDrawable(getResources().getDrawable(R.drawable.circle_violet));
        Utils.setCircleColour(DetailedBookActivitys.this, timeOne);

        textOne.setText(statusOne);
        textTwo.setText(statusTwo);
        textThree.setText(statusThree);
        textFour.setText(statusFour);
        textFive.setText(statusFive);
        textSix.setText(statusSix);


        viewOne.setVisibility(View.INVISIBLE);
        viewTwo.setVisibility(View.INVISIBLE);
        viewThree.setVisibility(View.INVISIBLE);
        viewFour.setVisibility(View.INVISIBLE);
        viewFive.setVisibility(View.INVISIBLE);
        viewSix.setVisibility(View.INVISIBLE);
        viewSeven.setVisibility(View.INVISIBLE);
        viewEight.setVisibility(View.INVISIBLE);
        viewNine.setVisibility(View.INVISIBLE);
        viewTen.setVisibility(View.INVISIBLE);
        viewEleven.setVisibility(View.INVISIBLE);


    }


    public void setAccepted() {
        layOne.setVisibility(View.VISIBLE);
        layTwo.setVisibility(View.VISIBLE);
        layThree.setVisibility(View.GONE);
        layFour.setVisibility(View.GONE);
        layFive.setVisibility(View.GONE);
        laySix.setVisibility(View.GONE);

      /*  timeOne.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeTwo.setImageDrawable(getResources().getDrawable(R.drawable.circle_violet));*/

        textOne.setText(statusOne);
        textTwo.setText(statusTwo);
        textThree.setText(statusThree);
        textFour.setText(statusFour);
        textFive.setText(statusFive);
        textSix.setText(statusSix);


        viewOne.setVisibility(View.VISIBLE);
        viewTwo.setVisibility(View.VISIBLE);
        viewThree.setVisibility(View.INVISIBLE);
        viewFour.setVisibility(View.INVISIBLE);
        viewFive.setVisibility(View.INVISIBLE);
        viewSix.setVisibility(View.INVISIBLE);
        viewSeven.setVisibility(View.INVISIBLE);
        viewEight.setVisibility(View.INVISIBLE);
        viewNine.setVisibility(View.INVISIBLE);
        viewTen.setVisibility(View.INVISIBLE);
        viewEleven.setVisibility(View.INVISIBLE);

        Utils.setViewDot(DetailedBookActivitys.this, timeOne);

        Utils.setCircleColour(DetailedBookActivitys.this, timeTwo);
    }

    public void setStartToPlace() {
        layOne.setVisibility(View.VISIBLE);
        layTwo.setVisibility(View.VISIBLE);
        layThree.setVisibility(View.VISIBLE);
        layFour.setVisibility(View.GONE);
        layFive.setVisibility(View.GONE);
        laySix.setVisibility(View.GONE);

     /*   timeOne.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeTwo.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeThree.setImageDrawable(getResources().getDrawable(R.drawable.circle_violet));*/


        textOne.setText(statusOne);
        textTwo.setText(statusTwo);
        textThree.setText(statusThree);
        textFour.setText(statusFour);
        textFive.setText(statusFive);
        textSix.setText(statusSix);


        viewOne.setVisibility(View.VISIBLE);
        viewTwo.setVisibility(View.VISIBLE);
        viewThree.setVisibility(View.VISIBLE);
        viewFour.setVisibility(View.VISIBLE);
        viewFive.setVisibility(View.INVISIBLE);
        viewSix.setVisibility(View.INVISIBLE);
        viewSeven.setVisibility(View.INVISIBLE);
        viewEight.setVisibility(View.INVISIBLE);
        viewNine.setVisibility(View.INVISIBLE);
        viewTen.setVisibility(View.INVISIBLE);
        viewEleven.setVisibility(View.INVISIBLE);

        Utils.setViewDot(DetailedBookActivitys.this, timeOne);
        Utils.setViewDot(DetailedBookActivitys.this, timeTwo);

        Utils.setCircleColour(DetailedBookActivitys.this, timeThree);
    }


    public void setProviderArrived() {
        layOne.setVisibility(View.VISIBLE);
        layTwo.setVisibility(View.VISIBLE);
        layThree.setVisibility(View.VISIBLE);
        layFour.setVisibility(View.VISIBLE);
        layFive.setVisibility(View.GONE);
        laySix.setVisibility(View.GONE);

     /*   timeOne.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeTwo.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeThree.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeFour.setImageDrawable(getResources().getDrawable(R.drawable.circle_violet));*/

        textOne.setText(statusOne);
        textTwo.setText(statusTwo);
        textThree.setText(statusThree);
        textFour.setText(statusFour);
        textFive.setText(statusFive);
        textSix.setText(statusSix);


        viewOne.setVisibility(View.VISIBLE);
        viewTwo.setVisibility(View.VISIBLE);
        viewThree.setVisibility(View.VISIBLE);
        viewFour.setVisibility(View.VISIBLE);
        viewFive.setVisibility(View.VISIBLE);
        viewSix.setVisibility(View.INVISIBLE);
        viewSeven.setVisibility(View.INVISIBLE);
        viewEight.setVisibility(View.INVISIBLE);
        viewNine.setVisibility(View.INVISIBLE);
        viewTen.setVisibility(View.INVISIBLE);
        viewEleven.setVisibility(View.INVISIBLE);

        Utils.setViewDot(DetailedBookActivitys.this, timeOne);
        Utils.setViewDot(DetailedBookActivitys.this, timeTwo);
        Utils.setViewDot(DetailedBookActivitys.this, timeThree);

        Utils.setCircleColour(DetailedBookActivitys.this, timeFour);
    }

    public void setJobStarted() {
        layOne.setVisibility(View.VISIBLE);
        layTwo.setVisibility(View.VISIBLE);
        layThree.setVisibility(View.VISIBLE);
        layFour.setVisibility(View.VISIBLE);
        layFive.setVisibility(View.VISIBLE);
        laySix.setVisibility(View.GONE);

        /*timeOne.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeTwo.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeThree.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeFour.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeFive.setImageDrawable(getResources().getDrawable(R.drawable.circle_violet));*/

        textOne.setText(statusOne);
        textTwo.setText(statusTwo);
        textThree.setText(statusThree);
        textFour.setText(statusFour);
        textFive.setText(statusFive);
        textSix.setText(statusSix);


        viewOne.setVisibility(View.VISIBLE);
        viewTwo.setVisibility(View.VISIBLE);
        viewThree.setVisibility(View.VISIBLE);
        viewFour.setVisibility(View.VISIBLE);
        viewFive.setVisibility(View.VISIBLE);
        viewSix.setVisibility(View.VISIBLE);
        viewSeven.setVisibility(View.VISIBLE);
        viewEight.setVisibility(View.VISIBLE);
        viewNine.setVisibility(View.INVISIBLE);
        viewTen.setVisibility(View.INVISIBLE);
        viewEleven.setVisibility(View.INVISIBLE);

        Utils.setViewDot(DetailedBookActivitys.this, timeOne);
        Utils.setViewDot(DetailedBookActivitys.this, timeTwo);
        Utils.setViewDot(DetailedBookActivitys.this, timeThree);
        Utils.setViewDot(DetailedBookActivitys.this, timeFour);

        Utils.setCircleColour(DetailedBookActivitys.this, timeFive);
    }


    public void setJobCompleted() {
        layOne.setVisibility(View.VISIBLE);
        layTwo.setVisibility(View.VISIBLE);
        layThree.setVisibility(View.VISIBLE);
        layFour.setVisibility(View.VISIBLE);
        layFive.setVisibility(View.VISIBLE);
        laySix.setVisibility(View.VISIBLE);

       /* timeOne.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeTwo.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeThree.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeFour.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeFive.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeSix.setImageDrawable(getResources().getDrawable(R.drawable.circle_violet));*/

        textOne.setText(statusOne);
        textTwo.setText(statusTwo);
        textThree.setText(statusThree);
        textFour.setText(statusFour);
        textFive.setText(statusFive);
        textSix.setText(statusSix);


        viewOne.setVisibility(View.VISIBLE);
        viewTwo.setVisibility(View.VISIBLE);
        viewThree.setVisibility(View.VISIBLE);
        viewFour.setVisibility(View.VISIBLE);
        viewFive.setVisibility(View.VISIBLE);
        viewSix.setVisibility(View.VISIBLE);
        viewSeven.setVisibility(View.VISIBLE);
        viewEight.setVisibility(View.VISIBLE);
        viewNine.setVisibility(View.VISIBLE);
        viewTen.setVisibility(View.VISIBLE);
        viewEleven.setVisibility(View.INVISIBLE);

        Utils.setViewDot(DetailedBookActivitys.this, timeOne);
        Utils.setViewDot(DetailedBookActivitys.this, timeTwo);
        Utils.setViewDot(DetailedBookActivitys.this, timeThree);
        Utils.setViewDot(DetailedBookActivitys.this, timeFour);
        Utils.setViewDot(DetailedBookActivitys.this, timeFive);

        Utils.setCircleColour(DetailedBookActivitys.this, timeSix);
    }


    private void getStaticMap(String lat, String longg) {

//        String urls="https://maps.googleapis.com/maps/api/staticmap?key="+getResources().getString(R.string.google_maps_key)+"&markers=color:red%7C"+lat+","+longg+"&center="+lat+","+longg+"&zoom=15&format=jpeg&maptype=roadmap&size=512x512&sensor=false";
        String urls = "https://maps.googleapis.com/maps/api/staticmap?key=" + getResources().getString(R.string.google_maps_key) + "&center=" + lat + "," + longg + "&zoom=15&format=jpeg&maptype=roadmap&size=512x512&sensor=false";
        Utils.log(TAG, "url: " + urls);
//        String url = "http://maps.google.com/maps/api/staticmap?center="+lat+","+longg+"&zoom=15&size=512x512&sensor=false";

        Glide.with(DetailedBookActivitys.this).load(urls).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                return false;
            }
        }).into(mapData);


    }


}
