package com.app.reparerprovider.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;


import com.app.reparerprovider.R;
import com.app.reparerprovider.Volley.ApiCall;
import com.app.reparerprovider.Volley.VolleyCallback;
import com.app.reparerprovider.helpers.AppSettings;
import com.app.reparerprovider.helpers.SharedHelper;
import com.app.reparerprovider.helpers.UrlHelper;
import com.app.reparerprovider.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by karthik on 01/10/17.
 */
public class SignInActivity extends BaseActivity implements View.OnClickListener {
    EditText usernameEditText, passwordEditText;
    Button dontHaveAnAccount, loginButton, forgotPassword;
    ImageView facebookLogin, googleLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(SignInActivity.this, "theme_value");
        Utils.SetTheme(SignInActivity.this, theme_value);
        setContentView(R.layout.activity_signin);
        initViews();
        initListners();
    }

    InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (Character.isWhitespace(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }

    };

    public class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new AsteriskPasswordTransformationMethod.PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }

    private void initViews() {
        dontHaveAnAccount = (Button) findViewById(R.id.dontHaveAnAccount);
        loginButton = (Button) findViewById(R.id.loginButton);
        Utils.setCustomButton(SignInActivity.this, loginButton);
        forgotPassword = (Button) findViewById(R.id.forgotPassword);
        usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        passwordEditText.setFilters(new InputFilter[]{filter});
        passwordEditText.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        facebookLogin = (ImageView) findViewById(R.id.facebookLogin);
        googleLogin = (ImageView) findViewById(R.id.googleLogin);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 106);
            }
        }

    }

    private void initListners() {
        dontHaveAnAccount.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 106) {
            if (grantResults.length > 0) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
        }
    }


    @Override
    public void onClick(View view) {
        if (view == dontHaveAnAccount) {
            moveSignUp();
        } else if (view == loginButton) {
            if (isValidInputs().equalsIgnoreCase("true")) {
                processLogin();
            } else {
                Utils.toast(SignInActivity.this, isValidInputs());
            }

        } else if (view == forgotPassword) {
            moveToForgotPassword();
        }
    }

    private void moveToForgotPassword() {
        Intent intent = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private String isValidInputs() {
        String value;
        if (usernameEditText.getText().length() == 0 || !isValidEmail(usernameEditText.getText().toString())) {
            value = getResources().getString(R.string.please_enter_valid_email);
        } else if (passwordEditText.getText().length() == 0) {
            value = getResources().getString(R.string.please_enter_valid_password);
        } else {
            value = "true";
        }
        return value;
    }

    private void processLogin() {
        ApiCall.PostMethod(SignInActivity.this, UrlHelper.SIGN_IN, getSignInInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                AppSettings appSettings = new AppSettings(SignInActivity.this);
                appSettings.setToken(response.optString("access_token"));
                appSettings.setProviderId(response.optString("provider_id"));
                appSettings.setIsLogged("true");
                moveMainActivity();

            }


        });
    }

    private void moveLocationSelectionActivity() {

        Intent main = new Intent(SignInActivity.this, SelectLocationActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(main);
        finish();

    }

    private JSONObject getSignInInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", usernameEditText.getText().toString().trim());
            jsonObject.put("password", passwordEditText.getText().toString());
            jsonObject.put("user_type", "Provider");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;

    }

    private void moveMainActivity() {
        Intent main = new Intent(SignInActivity.this, MainActivity.class);
        main.putExtra("type", "new");
        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(main);
        finish();
    }

    public void moveSignUp() {
        Intent signup = new Intent(SignInActivity.this, RegisterActivity.class);
        startActivity(signup);
        finish();
    }
}
