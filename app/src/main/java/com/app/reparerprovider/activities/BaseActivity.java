package com.app.reparerprovider.activities;

import android.support.v7.app.AppCompatActivity;

import com.app.reparerprovider.helpers.LocaleUtils;

public class BaseActivity extends AppCompatActivity {

    public BaseActivity() {
        LocaleUtils.updateConfig(this);
    }
} 