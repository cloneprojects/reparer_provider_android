package com.app.reparerprovider.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.app.reparerprovider.R;
import com.app.reparerprovider.Volley.ApiCall;
import com.app.reparerprovider.Volley.VolleyCallback;
import com.app.reparerprovider.helpers.SharedHelper;
import com.app.reparerprovider.helpers.UrlHelper;
import com.app.reparerprovider.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ResetPasswordActivity extends BaseActivity implements View.OnClickListener {
    EditText confirmPassword, newPassword;
    ImageView backButton;
    String email;
    Button saveButton;
    InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (Character.isWhitespace(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(ResetPasswordActivity.this, "theme_value");
        Utils.SetTheme(ResetPasswordActivity.this, theme_value);
        setContentView(R.layout.activity_reset_password);
        getIntentValues();
        initViews();
        initListners();
    }

    private void getIntentValues() {
        Intent intent = getIntent();
        email = intent.getStringExtra("emailValue");
    }

    private void initListners() {
        backButton.setOnClickListener(this);
        saveButton.setOnClickListener(this);
        confirmPassword.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        newPassword.setTransformationMethod(new AsteriskPasswordTransformationMethod());


        newPassword.setFilters(new InputFilter[]{filter});
        confirmPassword.setFilters(new InputFilter[]{filter});

    }

    private void initViews() {
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        newPassword = (EditText) findViewById(R.id.newPassword);
        backButton = (ImageView) findViewById(R.id.backButton);
        saveButton = (Button) findViewById(R.id.saveButton);
        Utils.setCustomButton(ResetPasswordActivity.this, saveButton);
    }

    @Override
    public void onClick(View view) {

        if (view == backButton) {
            onBackPressed();
        } else {
            if (validateInputs().equalsIgnoreCase("true")) {
                resetPassword();
            } else {
                Utils.toast(ResetPasswordActivity.this, validateInputs());
            }
        }
    }

    private String validateInputs() {
        String message = "true";

        if (newPassword.getText().length() == 0) {
            return getResources().getString(R.string.enter_password);

        } else if (confirmPassword.getText().length() == 0) {
            return getResources().getString(R.string.enter_confirmpassword);

        } else if (!newPassword.getText().toString().equalsIgnoreCase(confirmPassword.getText().toString())) {
            return getResources().getString(R.string.password_not_match);

        } else if (newPassword.getText().toString().trim().length() < 6) {
            return getResources().getString(R.string.password_must_be_six);

        } else if (confirmPassword.getText().toString().trim().length() < 6) {
            return getResources().getString(R.string.password_must_be_six);
        } else {
            return message;
        }
    }

    private void resetPassword() {
        ApiCall.PostMethod(ResetPasswordActivity.this, UrlHelper.RESET_PASSWORD, getInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                moveSignActivity();

            }
        });
    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("password", newPassword.getText().toString());
            jsonObject.put("email", email);
            jsonObject.put("confirmpassword", confirmPassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void moveSignActivity() {
        Utils.toast(ResetPasswordActivity.this, getResources().getString(R.string.password_changed_success));
        Intent intent = new Intent(ResetPasswordActivity.this, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new AsteriskPasswordTransformationMethod.PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }
}
