package com.app.reparerprovider.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;


import com.app.reparerprovider.R;
import com.app.reparerprovider.Volley.ApiCall;
import com.app.reparerprovider.Volley.VolleyCallback;
import com.app.reparerprovider.helpers.SharedHelper;
import com.app.reparerprovider.helpers.UrlHelper;
import com.app.reparerprovider.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {
    EditText confirmPassword, newPassword, oldPassword;
    ImageView backButton;
    String email;
    Button saveButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(ChangePasswordActivity.this, "theme_value");
        Utils.SetTheme(ChangePasswordActivity.this, theme_value);
        setContentView(R.layout.activity_change_password);
        getIntentValues();
        initViews();
        initListners();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
    }

    public class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }

    private void getIntentValues() {
        Intent intent = getIntent();
        email = intent.getStringExtra("emailValue");
    }

    private String validateInputs() {
        String message = "true";

        if (oldPassword.getText().length() == 0) {
            return getResources().getString(R.string.enter_password);

        } else if (newPassword.getText().length() == 0) {
            return getResources().getString(R.string.enter_newpassword);

        } else if (confirmPassword.getText().length() == 0) {
            return getResources().getString(R.string.enter_confirmpassword);

        } else if (oldPassword.getText().toString().trim().length() < 6) {
            return getResources().getString(R.string.password_must_be_six);

        } else if (newPassword.getText().toString().trim().length() < 6) {
            return getResources().getString(R.string.password_must_be_six);

        } else if (confirmPassword.getText().toString().trim().length() < 6) {
            return getResources().getString(R.string.password_must_be_six);

        } else if (!newPassword.getText().toString().equalsIgnoreCase(confirmPassword.getText().toString())) {
            return getResources().getString(R.string.password_not_match);

        } else {
            return message;
        }
    }

    InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (Character.isWhitespace(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }

    };

    private void initListners() {
        backButton.setOnClickListener(this);
        saveButton.setOnClickListener(this);
    }

    private void initViews() {
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        oldPassword = (EditText) findViewById(R.id.oldPassword);
        newPassword = (EditText) findViewById(R.id.newPassword);
        backButton = (ImageView) findViewById(R.id.backButton);
        saveButton = (Button) findViewById(R.id.saveButton);
        Utils.setCustomButton(ChangePasswordActivity.this, saveButton);
        confirmPassword.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        oldPassword.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        newPassword.setTransformationMethod(new AsteriskPasswordTransformationMethod());

        newPassword.setFilters(new InputFilter[]{filter});
        confirmPassword.setFilters(new InputFilter[]{filter});
        oldPassword.setFilters(new InputFilter[]{filter});

    }

    @Override
    public void onClick(View view) {

        if (view == backButton) {
            onBackPressed();
        } else {
            if (validateInputs().equalsIgnoreCase("true")) {
                resetPassword();
            } else {
                Utils.toast(ChangePasswordActivity.this, validateInputs());
            }

        }
    }

    private void resetPassword() {
        ApiCall.PostMethodHeaders(ChangePasswordActivity.this, UrlHelper.CHANGE_PASSWORD, getInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                moveMainActivity();

            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void moveMainActivity() {
        Utils.toast(ChangePasswordActivity.this, getResources().getString(R.string.password_changed_success));
        Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
        intent.putExtra("type", "new");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("oldpassword", oldPassword.getText().toString());
            jsonObject.put("newpassword", newPassword.getText().toString());
            jsonObject.put("cnfpassword", confirmPassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


}
