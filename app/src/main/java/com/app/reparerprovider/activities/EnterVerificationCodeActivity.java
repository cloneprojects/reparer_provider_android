package com.app.reparerprovider.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.app.reparerprovider.R;
import com.app.reparerprovider.helpers.SharedHelper;
import com.app.reparerprovider.helpers.Utils;


public class EnterVerificationCodeActivity extends BaseActivity implements View.OnClickListener {
    ImageView close;
    Button verifyButton;
    String email, otp;
    EditText otpEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(EnterVerificationCodeActivity.this, "theme_value");
        Utils.SetTheme(EnterVerificationCodeActivity.this, theme_value);
        setContentView(R.layout.activity_enter_verification_code);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }

        getIntentValues();
        initViews();
        initListners();
    }

    private void getIntentValues() {
        Intent intent = getIntent();
        email = intent.getStringExtra("emailValue");
        otp = intent.getStringExtra("otp");
    }

    private void initListners() {
        close.setOnClickListener(this);
        verifyButton.setOnClickListener(this);
    }

    private void initViews() {
        close = (ImageView) findViewById(R.id.close);
        verifyButton = (Button) findViewById(R.id.verifyButton);
        Utils.setCustomButton(EnterVerificationCodeActivity.this, verifyButton);
        otpEditText = (EditText) findViewById(R.id.otpEditText);
        otpEditText.setText(otp);
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            onBackPressed();
        } else if (view == verifyButton) {
            moveChangePassword();
        }
    }

    private void moveChangePassword() {
        Intent intent = new Intent(EnterVerificationCodeActivity.this, ResetPasswordActivity.class);
        intent.putExtra("emailValue", email);
        startActivity(intent);
    }
}
