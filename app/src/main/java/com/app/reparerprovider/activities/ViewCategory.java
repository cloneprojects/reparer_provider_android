package com.app.reparerprovider.activities;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.app.reparerprovider.R;
import com.app.reparerprovider.Volley.ApiCall;
import com.app.reparerprovider.Volley.VolleyCallback;
import com.app.reparerprovider.adapters.ViewProviderCategoryAdapter;
import com.app.reparerprovider.helpers.SharedHelper;
import com.app.reparerprovider.helpers.UrlHelper;
import com.app.reparerprovider.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ViewCategory extends BaseActivity {
    public static RecyclerView categoryItems;
    public static ViewProviderCategoryAdapter viewProviderCategoryAdapter;
    public static JSONArray categoryDetails = new JSONArray();
    public static JSONArray fullCategories = new JSONArray();
    public static JSONArray subCategories = new JSONArray();
    public static String[] categoryName;
    public static String[] subcategoryName;
    public static int subCategoryposition;
    public static int categoryPosition;
    public static int editPosition;
    public static boolean isEditenabled = false;
    private static String TAG = ViewCategory.class.getSimpleName();
    ImageView backButton;
    LinearLayout addCategory;

    public static JSONArray removeJsonObjectAtJsonArrayIndex(JSONArray source, int index) throws JSONException {
        if (index < 0 || index > source.length() - 1) {
            throw new IndexOutOfBoundsException();
        }

        final JSONArray copy = new JSONArray();
        for (int i = 0, count = source.length(); i < count; i++) {
            if (i != index) copy.put(source.get(i));
        }
        return copy;
    }

    public static void showEditDialog(final Context context, final JSONObject jsonObject, int position) {

        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.edit_category_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        final String catName, subCatName;
        catName = jsonObject.optString("categoryMainName");
        subCatName = jsonObject.optString("sub_category_name");

        final Spinner categorySpinner, subCategorySpinner;
        final EditText pricePerhour, quickPitch, experience;
        final Button confirmButton, deleteButton;
        categorySpinner = (Spinner) dialog.findViewById(R.id.categorySpinner);
        subCategorySpinner = (Spinner) dialog.findViewById(R.id.subCategorySpinner);
        pricePerhour = (EditText) dialog.findViewById(R.id.pricePerhour);
        quickPitch = (EditText) dialog.findViewById(R.id.quickPitch);
        experience = (EditText) dialog.findViewById(R.id.experience);
        confirmButton = (Button) dialog.findViewById(R.id.confirmButton);
        deleteButton = (Button) dialog.findViewById(R.id.deleteButton);

        Utils.setCustomButton(context, confirmButton);
        Utils.setCustomButton(context, deleteButton);

        experience.setText(jsonObject.optString("experience"));
        pricePerhour.setText(jsonObject.optString("priceperhour"));
        quickPitch.setText(jsonObject.optString("quickpitch"));

        isEditenabled = false;
        categorySpinner.setEnabled(false);
        subCategorySpinner.setEnabled(false);
        experience.setEnabled(false);
        pricePerhour.setEnabled(false);
        quickPitch.setEnabled(false);

        for (int i = 0; i < categoryName.length; i++) {
            Log.d(TAG, "showEditDialog: Loop" + categoryName[i] + ",cat:" + catName);

            if (categoryName[i].equalsIgnoreCase(catName)) {
                Log.d(TAG, "showEditDialog: " + categoryName[i] + ",cat:" + catName);
                categoryPosition = i;
            }
        }
        ArrayAdapter aa = new ArrayAdapter(context, R.layout.gender_items, categoryName);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(aa);
        categorySpinner.setSelection(categoryPosition, true);

        JSONObject input = new JSONObject();
        try {
            input.put("id", fullCategories.optJSONObject(categoryPosition).optString("id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodHeaders(context, UrlHelper.LIST_SUB_CATEGORY, input, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONObject category = new JSONObject();
                subCategories = response.optJSONArray("list_subcategory");
                subcategoryName = new String[subCategories.length()];
                List<String> list = new ArrayList<String>();
                for (int in = 0; in < subCategories.length(); in++) {

                    subcategoryName[in] = subCategories.optJSONObject(in).optString("sub_category_name");
                    if (subCategories.optJSONObject(in).optString("sub_category_name").equalsIgnoreCase(subCatName)) {
                        subCategoryposition = in;
                    }

                }
                ArrayAdapter aa = new ArrayAdapter(context, R.layout.gender_items, subcategoryName);
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                subCategorySpinner.setAdapter(aa);
                subCategorySpinner.setSelection(subCategoryposition, true);

            }
        });

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {

                JSONObject input = new JSONObject();
                try {
                    input.put("id", fullCategories.optJSONObject(i).optString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ApiCall.PostMethodHeaders(context, UrlHelper.LIST_SUB_CATEGORY, input, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        JSONObject category = new JSONObject();
                        subCategories = response.optJSONArray("list_subcategory");
                        subcategoryName = new String[subCategories.length()];
                        List<String> list = new ArrayList<String>();
                        for (int in = 0; in < subCategories.length(); in++) {

                            subcategoryName[in] = subCategories.optJSONObject(in).optString("sub_category_name");
                            if (subCategories.optJSONObject(in).optString("sub_category_name").equalsIgnoreCase(subCatName)) {
                                subCategoryposition = in;
                            }

                        }
                        ArrayAdapter aa = new ArrayAdapter(context, android.R.layout.simple_spinner_item, subcategoryName);
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        subCategorySpinner.setAdapter(aa);
                        subCategorySpinner.setSelection(subCategoryposition, true);

                    }
                });


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //                    jsonObject.put("status","0");
//                    categoryDetails = removeJsonObjectAtJsonArrayIndex(categoryDetails, editPosition);
//                    categoryDetails.put(jsonObject);
//                    viewProviderCategoryAdapter.refresh();
                Log.d(TAG, "onClick: " + categoryDetails);
                try {
                    deleteValues(dialog, jsonObject.optString("id"), context);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                if (isEditenabled) {
                    if (quickPitch.getText().length() > 0) {
                        if (pricePerhour.getText().length() > 0) {
                            if (experience.getText().length() > 0) {
                                if (categoryName[categorySpinner.getSelectedItemPosition()].length() > 0) {
                                    if (subcategoryName[subCategorySpinner.getSelectedItemPosition()].length() > 0) {
                                        JSONObject jsonObjecte = new JSONObject();
                                        JSONObject jsonObject1 = new JSONObject();
                                        try {
//                                        jsonObjecte.put("categoryMainName", categoryName[categorySpinner.getSelectedItemPosition()]);
//                                        jsonObjecte.put("sub_category_name", subcategoryName[subCategorySpinner.getSelectedItemPosition()]);
//                                        jsonObjecte.put("sub_category_id", subCategories.optJSONObject(subCategorySpinner.getSelectedItemPosition()).optString("id"));
//                                        jsonObjecte.put("quickpitch", quickPitch.getText().toString());
//                                        jsonObjecte.put("priceperhour", pricePerhour.getText().toString());
//                                        jsonObjecte.put("experience", experience.getText().toString());
//                                        jsonObjecte.put("category_id", fullCategories.optJSONObject(categorySpinner.getSelectedItemPosition()).optString("id"));
//                                        jsonObjecte.put("status", "1");
//                                        categoryDetails = removeJsonObjectAtJsonArrayIndex(categoryDetails, editPosition);
//                                        categoryDetails.put(jsonObjecte);
//
//                                        viewProviderCategoryAdapter.refresh();
//                                        viewProviderCategoryAdapter = new ViewProviderCategoryAdapter(context, categoryDetails);
//                                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
//                                        categoryItems.setLayoutManager(linearLayoutManager);
//                                        categoryItems.setAdapter(viewProviderCategoryAdapter);

                                            updateValues(dialog, context, pricePerhour.getText().toString(), experience.getText().toString(), quickPitch.getText().toString(), jsonObject.optString("id"));

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    } else {
                                        Utils.toast(context, context.getResources().getString(R.string.please_select_subcategroy));

                                    }
                                } else {
                                    Utils.toast(context, context.getResources().getString(R.string.please_select_categroy));

                                }
                            } else {
                                Utils.toast(context, context.getResources().getString(R.string.please_enter_experience));
                            }
                        } else {
                            Utils.toast(context, context.getResources().getString(R.string.please_enter_price_per_ho));
                        }
                    } else {
                        Utils.toast(context, context.getResources().getString(R.string.please_enter_quick_pitch));
                    }
                } else {
                    confirmButton.setText(context.getResources().getString(R.string.save));
                    isEditenabled = true;
                    categorySpinner.setEnabled(false);
                    subCategorySpinner.setEnabled(false);
                    experience.setEnabled(true);
                    pricePerhour.setEnabled(true);
                    quickPitch.setEnabled(true);
                }
            }
        });


    }

    private static void deleteValues(final Dialog dialog, String id, final Context context) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("provider_service_id", id);
        ApiCall.PostMethodHeaders(context, UrlHelper.DELETE_CATEGORY, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                dialog.dismiss();
                getValues(context);
            }
        });
    }

    private static void updateValues(final Dialog dialog, final Context context, String priceper, String experience, String quickpitch, String id) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("provider_service_id", id);
        jsonObject.put("priceperhour", priceper);
        jsonObject.put("quickpitch", quickpitch);
        jsonObject.put("experience", experience);
        ApiCall.PostMethodHeaders(context, UrlHelper.EDIT_CATEGORY, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                dialog.dismiss();
                getValues(context);
            }
        });
    }

    private static void getValues(final Context context) {
        ApiCall.getMethodHeaders(context, UrlHelper.VIEW_CATEGORY, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                categoryDetails = response.optJSONArray("category");
                Utils.log(TAG, "response:" + categoryDetails.toString());

                for (int i = 0; i < categoryDetails.length(); i++) {
                    try {
                        categoryDetails.optJSONObject(i).put("categoryMainName", categoryDetails.optJSONObject(i).optString("category_name"));
                        categoryDetails.optJSONObject(i).put("status", "1");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                viewProviderCategoryAdapter = new ViewProviderCategoryAdapter(context, categoryDetails);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                categoryItems.setLayoutManager(linearLayoutManager);
                categoryItems.setAdapter(viewProviderCategoryAdapter);

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(ViewCategory.this, "theme_value");
        Utils.SetTheme(ViewCategory.this, theme_value);
        setContentView(R.layout.activity_view_category);
        try {
            getCategoryData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initViews();

        getValues(ViewCategory.this);
        initListners();


    }

    private void initListners() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddDialog();

            }
        });
    }

    private void getCategoryData() throws JSONException {
        ApiCall.getMethod(ViewCategory.this, UrlHelper.LIST_CATEGORY, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray list_category = new JSONArray();
                fullCategories = response.optJSONArray("list_category");

                categoryName = new String[fullCategories.length()];
                for (int i = 0; i < fullCategories.length(); i++) {
                    categoryName[i] = fullCategories.optJSONObject(i).optString("category_name");


                }
            }
        });


    }

    private void showAddDialog() {
        final Dialog dialog = new Dialog(ViewCategory.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.add_category_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();


        final Spinner categorySpinner, subCategorySpinner;
        final EditText pricePerhour, quickPitch, experience;
        Button confirmButton;
        categorySpinner = (Spinner) dialog.findViewById(R.id.categorySpinner);
        subCategorySpinner = (Spinner) dialog.findViewById(R.id.subCategorySpinner);
        pricePerhour = (EditText) dialog.findViewById(R.id.pricePerhour);
        quickPitch = (EditText) dialog.findViewById(R.id.quickPitch);
        experience = (EditText) dialog.findViewById(R.id.experience);
        confirmButton = (Button) dialog.findViewById(R.id.confirmButton);
        Utils.setCustomButton(ViewCategory.this, confirmButton);
        ArrayAdapter aa = new ArrayAdapter(this, R.layout.gender_items, categoryName);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(aa);

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {


//                subCategories = fullCategories.optJSONObject(i).optJSONArray("list_subcategory");
//                subcategoryName = new String[subCategories.length()];
//                if (subCategories.length() == 0) {
//                    subcategoryName = new String[1];
//
//                    subcategoryName[0] = "";
//                    ArrayAdapter aa = new ArrayAdapter(ViewCategory.this, R.layout.gender_items, subcategoryName);
//                    aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                    subCategorySpinner.setAdapter(aa);
//                } else {
//                    for (int count = 0; count < subCategories.length(); count++) {
//
//                        subcategoryName[count] = subCategories.optJSONObject(count).optString("sub_category_name");
//                        ArrayAdapter aa = new ArrayAdapter(ViewCategory.this, android.R.layout.simple_spinner_item, subcategoryName);
//                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                        subCategorySpinner.setAdapter(aa);
//
//                    }
//                }


                JSONObject input = new JSONObject();
                try {
                    input.put("id", fullCategories.optJSONObject(i).optString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ApiCall.PostMethodHeaders(ViewCategory.this, UrlHelper.LIST_SUB_CATEGORY, input, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        JSONObject category = new JSONObject();
                        subCategories = response.optJSONArray("list_subcategory");
                        subcategoryName = new String[subCategories.length()];
                        List<String> list = new ArrayList<String>();
                        for (int in = 0; in < subCategories.length(); in++) {

                            subcategoryName[in] = subCategories.optJSONObject(in).optString("sub_category_name");

                        }
                        ArrayAdapter aa = new ArrayAdapter(ViewCategory.this, android.R.layout.simple_spinner_item, subcategoryName);
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        subCategorySpinner.setAdapter(aa);

                    }
                });


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quickPitch.getText().length() > 0) {
                    if (pricePerhour.getText().length() > 0) {
                        if (experience.getText().length() > 0) {
                            if (categoryName[categorySpinner.getSelectedItemPosition()].length() > 0) {
                                if (subcategoryName.length > 0) {
                                    if (subcategoryName[subCategorySpinner.getSelectedItemPosition()].length() > 0) {
                                        JSONObject jsonObject = new JSONObject();
                                        JSONObject jsonObject1 = new JSONObject();
                                        try {
                                            jsonObject.put("sub_category_id", subCategories.optJSONObject(subCategorySpinner.getSelectedItemPosition()).optString("id"));
                                            jsonObject.put("quickpitch", quickPitch.getText().toString());
                                            jsonObject.put("priceperhour", pricePerhour.getText().toString());
                                            jsonObject.put("experience", experience.getText().toString());
                                            jsonObject.put("category_id", fullCategories.optJSONObject(categorySpinner.getSelectedItemPosition()).optString("id"));


//                                        viewProviderCategoryAdapter = new ViewProviderCategoryAdapter(ViewCategory.this, categoryDetails);
//                                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ViewCategory.this, LinearLayoutManager.HORIZONTAL, false);
//                                        categoryItems.setLayoutManager(linearLayoutManager);
//                                        categoryItems.setAdapter(viewProviderCategoryAdapter);
                                            addValues(jsonObject, dialog);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        dialog.dismiss();
                                    } else {
                                        Utils.toast(ViewCategory.this, getResources().getString(R.string.please_select_subcategroy));

                                    }
                                }
                            } else {
                                Utils.toast(ViewCategory.this, getResources().getString(R.string.please_select_categroy));

                            }
                        } else {
                            Utils.toast(ViewCategory.this, getResources().getString(R.string.please_enter_experience));
                        }
                    } else {
                        Utils.toast(ViewCategory.this, getResources().getString(R.string.please_enter_price_per_ho));
                    }
                } else {
                    Utils.toast(ViewCategory.this, getResources().getString(R.string.please_enter_quick_pitch));
                }
            }
        });
    }

    private void addValues(JSONObject jsonObject, final Dialog dialog) {
        ApiCall.PostMethodHeaders(ViewCategory.this, UrlHelper.ADD_CATEGORY, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                dialog.dismiss();
                getValues(ViewCategory.this);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void initViews() {
        categoryItems = (RecyclerView) findViewById(R.id.categoryItems);
        backButton = (ImageView) findViewById(R.id.backButton);
        addCategory = (LinearLayout) findViewById(R.id.addCategory);
        CircleImageView add_category = (CircleImageView) findViewById(R.id.add_category);
        Utils.setCircleImageView(ViewCategory.this, add_category);
    }
//
//    public static void sendValues(final Context context) throws JSONException {
//        JSONObject jsonObject=new JSONObject();
//        jsonObject.put("provider_service_id",);
//        ApiCall.PostMethodHeaders(context, UrlHelper.UPDATE_CATEGORY, jsonObject, new VolleyCallback() {
//                    @Override
//                    public void onSuccess(JSONObject response) {
//                       getValues(context);
//                    }
//                }
//        );
//    }
}
