package com.app.reparerprovider.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.reparerprovider.R;
import com.app.reparerprovider.activities.RegisterActivity;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by karthik on 12/10/17.
 */
public class RegisterCategoryAdapter extends RecyclerView.Adapter<RegisterCategoryAdapter.MyViewHolder> {
    private Context context;
    private JSONArray categoryDetails;

    public RegisterCategoryAdapter(Context context, JSONArray categoryDetails) {
        this.context = context;
        this.categoryDetails = categoryDetails;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView categoryName, experience, pricePerHour, quickPitch, categoryMainName;
        ImageView removeIcon;


        MyViewHolder(View view) {
            super(view);
            categoryName = (TextView) view.findViewById(R.id.categoryName);
            experience = (TextView) view.findViewById(R.id.experience);
            pricePerHour = (TextView) view.findViewById(R.id.pricePerHour);
            quickPitch = (TextView) view.findViewById(R.id.quickPitch);
            categoryMainName = (TextView) view.findViewById(R.id.categoryMainName);
            removeIcon =view.findViewById(R.id.removeIcon);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_items_register, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        JSONObject jsonObject = new JSONObject();


        jsonObject = categoryDetails.optJSONObject(position);

        String catname = jsonObject.optString("sub_category_name");
        String experience = jsonObject.optString("experience");
        String priceperhour = jsonObject.optString("priceperhour");
        String quickpitch = jsonObject.optString("quickpitch");
        String categoryMainName = jsonObject.optString("categoryMainName");

        holder.categoryName.setText(catname);
        holder.experience.setText(experience);
        holder.pricePerHour.setText(priceperhour);
        holder.quickPitch.setText(quickpitch);
        holder.categoryMainName.setText(categoryMainName);
        holder.removeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegisterActivity.removeObject(position,context);
            }
        });


    }


    @Override
    public int getItemCount() {
        return categoryDetails.length();
    }
}
