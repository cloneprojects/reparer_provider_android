package com.app.reparerprovider.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.reparerprovider.R;
import com.app.reparerprovider.activities.ViewSchedule;
import com.app.reparerprovider.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by karthik on 12/10/17.
 */
public class ScheduleSlotAdapter extends RecyclerView.Adapter<ScheduleSlotAdapter.MyViewHolder> {
    public static JSONArray categoryDetails;
    int selectedtime;
    private Context context;

    public ScheduleSlotAdapter(Context context, JSONArray categoryDetails, int selectedTime) {
        this.context = context;
        this.categoryDetails = categoryDetails;
        this.selectedtime = selectedTime;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.time_slot_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        JSONObject jsonObject = new JSONObject();
        jsonObject = categoryDetails.optJSONObject(position);

        String time_slot = jsonObject.optString("timing");
        final String experience = jsonObject.optString("selected");
        holder.timeSlot.setText(time_slot);

        if (experience.equalsIgnoreCase("false")) {
            holder.backgroundLay.setBackground(context.getResources().getDrawable(R.drawable.timeslotwhite));
        } else {
            holder.backgroundLay.setBackground(Utils.getRLayout(context));

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (experience.equalsIgnoreCase("false")) {

                    try {
                        categoryDetails.optJSONObject(position).put("selected", "true");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    for (int i = 0; i < ViewSchedule.finalArray.length(); i++) {

                        if (i % 7 == selectedtime) {
                            Utils.log("checking", "values: " + ViewSchedule.finalArray.optJSONObject(i).optString("time_Slots_id") + "," + categoryDetails.optJSONObject(position).optString("id"));

                            if (ViewSchedule.finalArray.optJSONObject(i).optString("time_Slots_id").equalsIgnoreCase(categoryDetails.optJSONObject(position).optString("id"))) {
                                Utils.log("checking", ":insidebefore " + ViewSchedule.finalArray.optJSONObject(i));

                                try {
                                    ViewSchedule.finalArray.optJSONObject(i).put("status", "1");
                                    ViewSchedule.finalArray.optJSONObject(i).put("selected_text", categoryDetails.optJSONObject(position).optString("timing"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Utils.log("checking", ":insideafter " + ViewSchedule.finalArray.optJSONObject(i));

                            }

                        }
                    }

                } else {
                    try {
                        categoryDetails.optJSONObject(position).put("selected", "false");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    for (int i = 0; i < ViewSchedule.finalArray.length(); i++) {

                        if (i % 7 == selectedtime) {
                            Utils.log("checking", "values: " + ViewSchedule.finalArray.optJSONObject(i).optString("time_Slots_id") + "," + categoryDetails.optJSONObject(position).optString("id"));
                            if (ViewSchedule.finalArray.optJSONObject(i).optString("time_Slots_id").equalsIgnoreCase(categoryDetails.optJSONObject(position).optString("id"))) {
                                Utils.log("checking", ":insidebefore " + ViewSchedule.finalArray.optJSONObject(i));

                                try {
                                    ViewSchedule.finalArray.optJSONObject(i).put("status", "0");
                                    ViewSchedule.finalArray.optJSONObject(i).put("selected_text", categoryDetails.optJSONObject(position).optString("timing"));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Utils.log("checking", ":insideafter " + ViewSchedule.finalArray.optJSONObject(i));

                            }


                        }
                    }

                }
                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryDetails.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView timeSlot;
        LinearLayout backgroundLay;


        MyViewHolder(View view) {
            super(view);
            timeSlot = (TextView) view.findViewById(R.id.timeSlot);
            backgroundLay = (LinearLayout) view.findViewById(R.id.background);


        }
    }
}
