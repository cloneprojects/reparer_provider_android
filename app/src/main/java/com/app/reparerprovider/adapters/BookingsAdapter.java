package com.app.reparerprovider.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.reparerprovider.R;
import com.app.reparerprovider.Volley.ApiCall;
import com.app.reparerprovider.Volley.VolleyCallback;
import com.app.reparerprovider.activities.DetailedBookActivitys;
import com.app.reparerprovider.fragments.HomeFragment;
import com.app.reparerprovider.helpers.UrlHelper;
import com.app.reparerprovider.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by karthik on 12/10/17.
 */
public class BookingsAdapter extends RecyclerView.Adapter<BookingsAdapter.MyViewHolder> {
    private Context context;
    private JSONArray subCategories;

    public BookingsAdapter(Context context, JSONArray subCategories) {
        this.context = context;
        this.subCategories = subCategories;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_bookings, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {


            holder.serviceName.setText(subCategories.optJSONObject(position).optString("sub_category_name"));

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            Date outDate = dateFormatter.parse(subCategories.optJSONObject(position).optString("booking_date"));
            DateFormat formatter = new SimpleDateFormat("MMM dd yyyy");
            String dateVal = formatter.format(outDate);
            String providerName, eta, distance, phone_number = "", prvoider_pic;
            providerName = subCategories.optJSONObject(position).optString("Name");
            prvoider_pic = subCategories.optJSONObject(position).optString("userimage");
            phone_number = subCategories.optJSONObject(position).optString("user_mobile");


            holder.providerName.setText(providerName);

            holder.serviceDate.setText(dateVal);

            Glide.with(context).load(prvoider_pic).placeholder(context.getResources().getDrawable(R.drawable.profile_pic)).error(context.getResources().getDrawable(R.drawable.profile_pic)).into(holder.providerPic);

            holder.serviceTiming.setText(subCategories.optJSONObject(position).optString("timing"));
        } catch (Exception e) {

        }


        String status = subCategories.optJSONObject(position).optString("status");
        int colorvalue = 0;
        String statusValue = "";
        Drawable statusIcon = null;

//        String descValue = "";

//
        if (status.equalsIgnoreCase("Pending")) {
            holder.updateStatus.setVisibility(View.GONE);

            statusValue = context.getResources().getString(R.string.pending);
            colorvalue = context.getResources().getColor(R.color.status_orange);
            statusIcon = context.getResources().getDrawable(R.drawable.new_pending);


        } else if (status.equalsIgnoreCase("Rejected")) {

            statusValue = context.getResources().getString(R.string.rejected);
            holder.updateStatus.setVisibility(View.GONE);

            colorvalue = context.getResources().getColor(R.color.red);
            statusIcon = context.getResources().getDrawable(R.drawable.new_cancelled);


        } else if (status.equalsIgnoreCase("Accepted")) {
            statusValue = context.getResources().getString(R.string.accepted);
            colorvalue = context.getResources().getColor(R.color.green);
            statusIcon = context.getResources().getDrawable(R.drawable.new_accepted);
            holder.updateStatus.setVisibility(View.VISIBLE);
            holder.updateStatus.setText(context.getResources().getString(R.string.start_to_place));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.updateStatus.setBackgroundTintList(ColorStateList.valueOf(colorvalue));
            }

        } else if (status.equalsIgnoreCase("CancelledbyUser")) {
            statusValue = context.getResources().getString(R.string.cancelled) + " " + context.getResources().getString(R.string.by_user);
            holder.updateStatus.setVisibility(View.GONE);

            colorvalue = context.getResources().getColor(R.color.red);
            statusIcon = context.getResources().getDrawable(R.drawable.new_cancelled);

        } else if (status.equalsIgnoreCase("CancelledbyProvider")) {
            statusValue = context.getResources().getString(R.string.cancelled) + " " + context.getResources().getString(R.string.by_provider);
            holder.updateStatus.setVisibility(View.GONE);

            colorvalue = context.getResources().getColor(R.color.red);
            statusIcon = context.getResources().getDrawable(R.drawable.new_cancelled);


        } else if (status.equalsIgnoreCase("StarttoCustomerPlace")) {

            statusValue = context.getResources().getString(R.string.on_the_way);
            colorvalue = context.getResources().getColor(R.color.status_orange);
            statusIcon = context.getResources().getDrawable(R.drawable.new_start_to_place);
            holder.updateStatus.setVisibility(View.VISIBLE);
            holder.updateStatus.setText(context.getResources().getString(R.string.start_job));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.updateStatus.setBackgroundTintList(ColorStateList.valueOf(colorvalue));
            }

        } else if (status.equalsIgnoreCase("Startedjob")) {
            statusValue = context.getResources().getString(R.string.job_started);
            colorvalue = context.getResources().getColor(R.color.status_orange);
            statusIcon = context.getResources().getDrawable(R.drawable.new_started_job);
            holder.updateStatus.setVisibility(View.VISIBLE);
            holder.updateStatus.setText(context.getResources().getString(R.string.complete_job));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.updateStatus.setBackgroundTintList(ColorStateList.valueOf(colorvalue));
            }

        } else if (status.equalsIgnoreCase("Completedjob")) {
            statusValue = context.getResources().getString(R.string.completed_job);
            holder.updateStatus.setVisibility(View.GONE);

            colorvalue = context.getResources().getColor(R.color.green);
            statusIcon = context.getResources().getDrawable(R.drawable.new_complete_job);
        } else if (status.equalsIgnoreCase("Waitingforpaymentconfirmation")) {
            statusValue = context.getResources().getString(R.string.waiting_for);
            holder.updateStatus.setVisibility(View.GONE);

            colorvalue = context.getResources().getColor(R.color.status_orange);
            statusIcon = context.getResources().getDrawable(R.drawable.new_pay_conifrmation);
        } else if (status.equalsIgnoreCase("Reviewpending")) {
            statusValue = context.getResources().getString(R.string.review_pending);
            holder.updateStatus.setVisibility(View.GONE);

            colorvalue = context.getResources().getColor(R.color.ratingColor);
            statusIcon = context.getResources().getDrawable(R.drawable.new_review);
        } else if (status.equalsIgnoreCase("Finished")) {
            statusValue = context.getResources().getString(R.string.finished);
            holder.updateStatus.setVisibility(View.GONE);

            colorvalue = context.getResources().getColor(R.color.green);
            statusIcon = context.getResources().getDrawable(R.drawable.new_finished);

        }

        holder.statusText.setText(statusValue);
        holder.statusText.setTextColor(colorvalue);
        holder.statusIcon.setImageDrawable(statusIcon);

        holder.updateStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.updateStatus.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.start_to_place)))
                {
                    updateStatus(subCategories.optJSONObject(position).optString("id"),"updatePlace");
                } else if (holder.updateStatus.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.start_job)))
                {
                    updateStatus(subCategories.optJSONObject(position).optString("id"),"startJob");

                } else {
                    updateStatus(subCategories.optJSONObject(position).optString("id"),"completeJob");

                }
            }
        });


//        String status = subCategories.optJSONObject(position).optString("status");
//
//        String descValue = "";
//
//        if (status.equalsIgnoreCase("Pending")) {
//            descValue = context.getResources().getString(R.string.pending_description);
//        } else if (status.equalsIgnoreCase("Rejected")) {
//            descValue = context.getResources().getString(R.string.rejected_description);
//
//
//        } else if (status.equalsIgnoreCase("Accepted")) {
//            descValue = context.getResources().getString(R.string.accepted_description);
//
//        } else if (status.equalsIgnoreCase("CancelledbyUser")) {
//            descValue = context.getResources().getString(R.string.user_cancel_description);
//        } else if (status.equalsIgnoreCase("CancelledbyProvider")) {
//            descValue = context.getResources().getString(R.string.provider_cancel_description);
//
//
//        } else if (status.equalsIgnoreCase("StarttoCustomerPlace")) {
//            descValue = context.getResources().getString(R.string.start_customer_description);
//
//        } else if (status.equalsIgnoreCase("Startedjob")) {
//            descValue = context.getResources().getString(R.string.start_job_description);
//
//
//        } else if (status.equalsIgnoreCase("Completedjob")) {
//            descValue = context.getResources().getString(R.string.complete_job_description);
//
//        } else if (status.equalsIgnoreCase("Waitingforpaymentconfirmation")) {
//            descValue = context.getResources().getString(R.string.payment_waiting_description);
//
//        } else if (status.equalsIgnoreCase("Reviewpending")) {
//            descValue = context.getResources().getString(R.string.review_pending_description);
//        } else if (status.equalsIgnoreCase("Finished")) {
//            descValue = context.getResources().getString(R.string.finished_description);
//
//        }
//
//        holder.bookingDescription.setText(descValue);
//
//
//        if (status.equalsIgnoreCase("Rejected")) {
//            setTextVisibility(1,holder.isInPending,holder.isOnTheWay,holder.isInProgress,holder.isCompleted);
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.rejected));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_grey));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_grey));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_grey));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.red));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.rejected));
//
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//            holder.startJob.setEnabled(false);
//            holder.cancel.setEnabled(false);
//            holder.startPlace.setEnabled(false);
//
//        } else if (status.equalsIgnoreCase("CancelledbyUser")) {
//            setTextVisibility(1,holder.isInPending,holder.isOnTheWay,holder.isInProgress,holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.rejected));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_grey));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_grey));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_grey));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.red));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.cancelled));
//
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//            holder.startJob.setEnabled(false);
//            holder.cancel.setEnabled(false);
//            holder.startPlace.setEnabled(false);
//
//        } else if (status.equalsIgnoreCase("CancelledbyProvider")) {
//            setTextVisibility(1,holder.isInPending,holder.isOnTheWay,holder.isInProgress,holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.rejected));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_grey));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_grey));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_grey));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.red));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.cancelled));
//
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//            holder.startJob.setEnabled(false);
//            holder.cancel.setEnabled(false);
//            holder.startPlace.setEnabled(false);
//
//        } else if (status.equalsIgnoreCase("Pending")) {
//            setTextVisibility(1,holder.isInPending,holder.isOnTheWay,holder.isInProgress,holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pending_orange));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_grey));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_grey));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_grey));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.orange));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.pending));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//
//
//            holder.startJob.setEnabled(false);
//            holder.cancel.setEnabled(false);
//            holder.startPlace.setEnabled(false);
//
//        } else if (status.equalsIgnoreCase("Accepted")) {
//            setTextVisibility(1,holder.isInPending,holder.isOnTheWay,holder.isInProgress,holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_blue));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_grey));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_grey));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_grey));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.blue));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//            holder.startJob.setEnabled(false);
//            holder.cancel.setEnabled(true);
//            holder.startPlace.setEnabled(true);
//        } else if (status.equalsIgnoreCase("StarttoCustomerPlace")) {
//            setTextVisibility(2,holder.isInPending,holder.isOnTheWay,holder.isInProgress,holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_black));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_orange));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_grey));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_grey));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.orange));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//
//
//            holder.startJob.setText(context.getResources().getString(R.string.start_job));
//
//
//            holder.startJob.setEnabled(true);
//            holder.cancel.setEnabled(false);
//            holder.startPlace.setEnabled(true);
//            holder.startPlace.setText(context.getResources().getString(R.string.get_directions));
//
//        } else if (status.equalsIgnoreCase("Startedjob")) {
//            setTextVisibility(3,holder.isInPending,holder.isOnTheWay,holder.isInProgress,holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_black));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_black));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_orange));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.complete));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.orange));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//
//            holder.startJob.setEnabled(true);
//            holder.startJob.setText(context.getResources().getString(R.string.completed_job));
//            holder.cancel.setEnabled(false);
//            holder.startPlace.setEnabled(false);
//        } else if (status.equalsIgnoreCase("Completedjob")) {
//            setTextVisibility(4,holder.isInPending,holder.isOnTheWay,holder.isInProgress,holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_black));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_black));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.job_blue));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_orange));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.blue));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.completed));
//            holder.isCompleted.setText(context.getResources().getString(R.string.review));
//            holder.startJob.setEnabled(false);
//            holder.cancel.setEnabled(false);
//            holder.startPlace.setEnabled(false);
//
//
//        } else if (status.equalsIgnoreCase("Waitingforpaymentconfirmation")) {
//            setTextVisibility(4,holder.isInPending,holder.isOnTheWay,holder.isInProgress,holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_black));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_black));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.job_black));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_orange));
//
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.orange));
//            holder.startJob.setEnabled(false);
//            holder.cancel.setEnabled(false);
//            holder.startPlace.setEnabled(false);
//
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.completed));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//        } else if (status.equalsIgnoreCase("Reviewpending")) {
//            setTextVisibility(4,holder.isInPending,holder.isOnTheWay,holder.isInProgress,holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_black));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_black));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.job_black));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_orange));
//
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.orange));
//            holder.startJob.setEnabled(false);
//            holder.cancel.setEnabled(false);
//            holder.startPlace.setEnabled(false);
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.completed));
//            holder.isCompleted.setText(context.getResources().getString(R.string.review));
//        } else {
//            setTextVisibility(4,holder.isInPending,holder.isOnTheWay,holder.isInProgress,holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_black));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_black));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.job_black));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.payment_blue));
//
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.blue));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.completed));
//            holder.isCompleted.setText(context.getResources().getString(R.string.finished));
//            holder.startJob.setEnabled(false);
//            holder.cancel.setEnabled(false);
//            holder.startPlace.setEnabled(false);
//
//        }
//        Glide.with(context).load(subCategories.optJSONObject(position).optString("icon")).into(holder.serviceImage);
//

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent detailedBooking = new Intent(context, DetailedBookActivitys.class);
                detailedBooking.putExtra("bookingValues", subCategories.optJSONObject(position).toString());
                context.startActivity(detailedBooking);
            }
        });
//
//        holder.startJob.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String status = "";
//
//                if (holder.startJob.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.start_place))) {
//                    status = "updatePlace";
//                }
//
//                if (holder.startJob.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.start_job))) {
//                    status = "startJob";
//                }
//                if (holder.startJob.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.completed_job))) {
//                    status = "completeJob";
//                }
//
//                updateStatus(subCategories.optJSONObject(position).optString("id"), status);
//            }
//        });
//
//        holder.startPlace.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String status = "";
//                status = "updatePlace";
//                if (subCategories.optJSONObject(position).optString("status").equalsIgnoreCase("StarttoCustomerPlace"))
//                {
//                    String src=subCategories.optJSONObject(position).optString("user_latitude");
//                    String des=subCategories.optJSONObject(position).optString("user_longitude");
//                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                            Uri.parse("http://maps.google.com/maps?daddr="+src+","+des));
//                    context.startActivity(intent);
//                }else {
//                    updateStatus(subCategories.optJSONObject(position).optString("id"), status);
//                }
//
//            }
//        });
//
//        holder.cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showCancelDialog(subCategories.optJSONObject(position).optString("id"));
//            }
//        });

    }

    private void showCancelDialog(final String id) {
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.cancel_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        TextView yesButton, noButton;
        yesButton = (TextView) dialog.findViewById(R.id.yesButton);
        noButton = (TextView) dialog.findViewById(R.id.noButton);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                canceBookings(id);
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void canceBookings(String id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodHeaders(context, UrlHelper.CANCEL_BOOKING, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                HomeFragment.getPendingRequests(context);
            }
        });
    }

    private void updateStatus(String id, String status) {
        String url = "";
        if (status.equalsIgnoreCase("updatePlace")) {
            url = UrlHelper.START_TO_PLACE;
        }

        if (status.equalsIgnoreCase("startJob")) {
            url = UrlHelper.START_JOB;
        }
        if (status.equalsIgnoreCase("completeJob")) {
            url = UrlHelper.COMPLETED_JOB;

        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodHeaders(context, url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                HomeFragment.getPendingRequests(context);
            }
        });


    }


    public void setTextVisibility(int num, TextView first, TextView second, TextView third, TextView four) {
        if (num == 1) {
            first.setVisibility(View.VISIBLE);
            second.setVisibility(View.GONE);
            third.setVisibility(View.GONE);
            four.setVisibility(View.GONE);
        } else if (num == 2) {
            first.setVisibility(View.VISIBLE);
            second.setVisibility(View.VISIBLE);
            third.setVisibility(View.GONE);
            four.setVisibility(View.GONE);
        } else if (num == 3) {
            first.setVisibility(View.VISIBLE);
            second.setVisibility(View.VISIBLE);
            third.setVisibility(View.VISIBLE);
            four.setVisibility(View.GONE);
        } else if (num == 4) {
            first.setVisibility(View.VISIBLE);
            second.setVisibility(View.VISIBLE);
            third.setVisibility(View.VISIBLE);
            four.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return subCategories.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView serviceName, serviceTiming, statusText, serviceDate,updateStatus;

        ImageView serviceImage, providerPic, statusIcon;

        TextView isInPending, isOnTheWay, isInProgress, isCompleted, bookingDescription, providerName, phoneNumber;

        ImageView isCompletedImage, isInProgressImage, isOnTheWayImage, isConfirmedImage;
//        RelativeLayout rootLayout;
        Button cancel, startJob, startPlace;

        MyViewHolder(View view) {
            super(view);
//            serviceName = view.findViewById(R.id.serviceName);
//            serviceTiming = view.findViewById(R.id.serviceTiming);
//            serviceImage = view.findViewById(R.id.serviceIcon);
//            rootLayout = view.findViewById(R.id.rootLayout);
//            isCompletedImage = view.findViewById(R.id.isCompletedImage);
//            isInProgressImage = view.findViewById(R.id.isInProgressImage);
//            isOnTheWayImage = view.findViewById(R.id.isOnTheWayImage);
//            isConfirmedImage = view.findViewById(R.id.isConfirmedImage);
//            isInPending = view.findViewById(R.id.isInPending);
//            isOnTheWay = view.findViewById(R.id.isOnTheWay);
//            bookingDescription = view.findViewById(R.id.bookingDescription);
//            isInProgress = view.findViewById(R.id.isInProgress);
//            isCompleted = view.findViewById(R.id.isCompleted);
//            startJob = view.findViewById(R.id.startJob);
//            cancel = view.findViewById(R.id.cancel);
//            startPlace = view.findViewById(R.id.startPlace);

            statusText = view.findViewById(R.id.statusText);
            providerPic = view.findViewById(R.id.providerPic);
            Utils.setProfilePicture(context,providerPic);
            providerName = view.findViewById(R.id.providerName);
            serviceName = view.findViewById(R.id.serviceName);
            serviceTiming = view.findViewById(R.id.serviceTiming);
            serviceDate = view.findViewById(R.id.serviceDate);
            statusIcon = view.findViewById(R.id.statusIcon);
            updateStatus = view.findViewById(R.id.updateStatus);
//            serviceImage = view.findViewById(R.id.serviceIcon);
//            rootLayout = view.findViewById(R.id.rootLayout);
        }
    }
}
